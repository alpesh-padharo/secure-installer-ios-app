//
//  ProductGuideViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 15/10/20.
//

import UIKit
import SafariServices
import EmptyDataSet_Swift
import SystemConfiguration


class ProductGuideViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    fileprivate var isSearching : Bool = false

    let kHeaderSectionTag: Int = 6900;

    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var tableHeaderLabel: UILabel!
    @IBOutlet weak var tableHeaderSearchTextfield: UITextField!{
        didSet {
            tableHeaderSearchTextfield.tintColor = UIColor.lightGray
            tableHeaderSearchTextfield.setIcon(#imageLiteral(resourceName: "zoom-out"))
         }
    }
    
    @IBOutlet weak var collectionHeaderLabel: UILabel!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    internal var arrayProduct = [Product]()
    fileprivate var arrayUniqueProduct = [Product]()
    fileprivate var arrayfilteredProductList: [Product] = []
    fileprivate var arrayProductList = [Product]()

    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView!.tableFooterView = UIView()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self

        
        setupUI()
        navBarModifications()
        setProductDataFromSelectedProduct()
    }
    private func setProductDataFromSelectedProduct() {
        
        arrayProductList = arrayProduct
        arrayUniqueProduct = arrayProductList.unique{$0.productModelName}
        arrayUniqueProduct = arrayProductList.filter { $0.manufacturerName!.contains("Secure") }
        
        sectionNames = ["Heating and Hot water controls"]
        
        sectionItems = [ ["TimeSwitches & programmers",
                          "Programmable thermostat",
                          "Motorised valves & actuators",
                          "Electric water heating",
                          "Thermostat"]
                        ];
        collectionView.reloadData()
        tableView.reloadData()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    fileprivate func setupUI() {
        tableHeaderLabel.font = UIFont(name: "Kohinoor Devanagari", size: 14)
        tableHeaderSearchTextfield.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        tableHeaderSearchTextfield.layer.borderWidth = 1
        tableHeaderSearchTextfield.layer.borderColor = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 241.0 / 255.0, alpha: 1).cgColor
        tableHeaderSearchTextfield.backgroundColor = .clear
        tableHeaderSearchTextfield.leftViewMode = .always
        tableHeaderSearchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
        // tableHeaderSearchTextfield.delegate = self
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if(textField.text?.count ?? 0 > 0) {
            isSearching = true
        } else {
            isSearching = false
        }
        arrayfilteredProductList = arrayUniqueProduct.filter { $0.productModelName!.localizedCaseInsensitiveContains(textField.text!) }
        collectionView.reloadData()
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
        self.title = "Product guide"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Tableview Methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
//            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
//            messageLabel.text = "Retrieving data.\nPlease wait."
//            messageLabel.numberOfLines = 0;
//            messageLabel.textAlignment = .center;
//            messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
//            messageLabel.sizeToFit()
//            self.tableView.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = hexStringToUIColor(hex: "#79265C")
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 16)!
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(ProductGuideViewController.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 14)!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = self.sectionItems[indexPath.section] as! NSArray
        let selectedCategory = section[indexPath.row] as? String
        if(!selectedCategory!.isEmpty)
        {
            navigateToProductList(selectedRecord: selectedCategory!)
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func navigateToProductList (selectedRecord : String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let productListViewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        productListViewController.selectedSubcategoryName = selectedRecord
        productListViewController.arrayProduct = arrayProduct
        productListViewController.isMovingFromProductGuideScreen = true
        self.navigationController?.pushViewController(productListViewController, animated: true)
    }
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as! UIImageView?
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as! UIImageView?
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }


}

extension ProductGuideViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (isSearching == false) {
            return arrayUniqueProduct.count
        } else {
            return arrayfilteredProductList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        cell.labelName.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
        cell.borderView.roundedView()

        if arrayfilteredProductList.count == 0 {
            cell.labelName.text = arrayUniqueProduct[indexPath.row].productModelName
            cell.imageView?.image = UIImage.convertBase64TypeToImage(imgString: arrayUniqueProduct[indexPath.row].image!)
        
        } else {
            cell.labelName.text = arrayfilteredProductList[indexPath.row].productModelName
            cell.imageView.image = UIImage.convertBase64TypeToImage(imgString: arrayfilteredProductList[indexPath.row].image!)
           
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showTutorial(indexPath.row)
    }
    
    func showTutorial(_ which: Int) {
        if(isConnectedToNetwork())
        {
            var web_link = "";
            if arrayfilteredProductList.count == 0 {
                web_link = arrayUniqueProduct[which].faqurl ?? "https://www.securemeters.com"
            } else {
                web_link = arrayfilteredProductList[which].faqurl ?? "https://www.securemeters.com"
            }
            
            
            if let url = URL(string: web_link) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Alert", message: "Please check your internet connection.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                  /*switch action.style{
                  case .default:
                        print("default")

                  case .cancel:
                        print("cancel")

                  case .destructive:
                        print("destructive")


            }*/}))
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    fileprivate func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }

}

extension ProductGuideViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 91, height: 85)
    }
    
}

extension ProductGuideViewController : EmptyDataSetSource, EmptyDataSetDelegate {
    
    //MARK: - DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No data found", attributes: [NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 19) as Any])
    }

    //MARK: - DZNEmptyDataSetDelegate Methods
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }

}
