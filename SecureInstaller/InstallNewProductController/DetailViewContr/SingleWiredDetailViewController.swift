//
//  SingleWiredDetailViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit
import SafariServices

class SingleWiredDetailViewController: UIViewController, UICollectionViewDelegate {

    var aProduct : Product = Product()

    @IBOutlet weak var tableSingleWired: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navBarModifications()
        tableSingleWired.delegate = self
        tableSingleWired.dataSource = self
        tableSingleWired.tableFooterView = UIView()
        
        var lastSearchedArray = DocumentFileManager.shared.getStoredlastSearched()
        if(!lastSearchedArray.contains(String(aProduct.id))){
            lastSearchedArray.append(String(aProduct.id))
            DocumentFileManager.shared.saveLastSearched(array: lastSearchedArray)
        }
        
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 17)!]
        self.title = "New Product Installation"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func testFunc(button: UIButton){
        showTutorial()
    }
    
    func showTutorial() {
        if let url = URL(string:K.ProductionServer.staticUrl) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SingleWiredDetailViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return 94
        } else if (indexPath.row == 1) {
            return 60
        }else if (indexPath.row == 2) {
            
            if(aProduct.instructions!.count <= 0) {
                return 37
            } else {
                return 137
            }
            
        }else if (indexPath.row == 3) {
            return 170
        }else if (indexPath.row == 4) {
            return 81
        } else {
            return 44
        }
        
    }
}

extension SingleWiredDetailViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            // ProductNameViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductNameViewCell") as? ProductNameViewCell
            
            cell?.LabelProductName?.text = aProduct.productModelName
            cell?.imageViewProduct?.image = UIImage.convertBase64TypeToImage(imgString: aProduct.image!)

            return cell ?? UITableViewCell()
        } else if (indexPath.row == 1) {
            // WiringAndBackplateCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "WiringAndBackplateCell") as? WiringAndBackplateCell
            cell?.labelValue.text = aProduct.wiringAndBackPlate
            return cell ?? UITableViewCell()
        }else if (indexPath.row == 2) {
            // InstructionsViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstructionsViewCell") as? InstructionsViewCell            
            let stringArray = aProduct.instructions!.components(separatedBy: "|").filter({ $0 != ""})
            
            cell?.textViewInstructions?.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))

            return cell ?? UITableViewCell()
        }else if (indexPath.row == 3) {
            // WiringDiagramViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "WiringDiagramViewCell") as? WiringDiagramViewCell
            cell?.collectionViewSingleWired.delegate = self
            cell?.collectionViewSingleWired.dataSource = self
            
            cell?.labelProductname.text = "Secure \(aProduct.productModelName ?? "")"
            
            return cell ?? UITableViewCell()
        } else if (indexPath.row == 4) {
            // InstallLinkCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "InstallLinkCell") as! InstallLinkCell?
            
            cell?.labelHeader?.text = "Follow the link below the guided installation"
            cell?.buttonUrl?.addTarget(self, action: #selector(testFunc(button:)), for: .touchUpInside)
            cell?.buttonUrl?.setTitle("Install \(aProduct.productModelName ?? "")", for: .normal)
            cell?.labelFooter?.text = "The link will redirect you to the secure webpage"
            return cell ?? UITableViewCell()
        } else {
            return UITableViewCell()
        }
    }
    
    
}
extension SingleWiredDetailViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 14, height: 70)
    }
    
}

extension SingleWiredDetailViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleWiredCollectionViewCell", for: indexPath) as! SingleWiredCollectionViewCell
        cell.labelTop.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
        cell.labelBottom.font = UIFont(name: "Kohinoor Devanagari", size: 6)!
        cell.labelBottom.adjustsFontSizeToFitWidth = true
        
        cell.labelTop.layer.backgroundColor  = UIColor.gray.cgColor
        cell.labelTop.layer.cornerRadius = 10
        cell.labelTop.layer.masksToBounds = true
        cell.labelTop.backgroundColor = UIColor.white
        
        cell.labelTop.layer.borderWidth = 1
        cell.labelTop.layer.borderColor = UIColor.gray.cgColor
        

        if(indexPath.row == 0) {
            
            cell.labelTop.text = "E"
            
            if(aProduct.pair1!.count <= 0){
                cell.labelBottom.text = "E"
            }else {
                cell.labelBottom.text = aProduct.pair1
            }
            
            
        } else if (indexPath.row == 1) {
            cell.labelTop.text = "N"
            if(aProduct.pair2!.count <= 0){
                cell.labelBottom.text = "N"
            } else {
                cell.labelBottom.text = aProduct.pair2
            }
            
        }else if (indexPath.row == 2) {
            cell.labelTop.text = "L"
            if(aProduct.pair3!.count <= 0){
                cell.labelBottom.text = "L"
            } else {
                cell.labelBottom.text = aProduct.pair3
            }
            
        }else if (indexPath.row == 3) {
            cell.labelTop.text = "1"
            if(aProduct.pair4!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair4
            }
        }else if (indexPath.row == 4) {
            cell.labelTop.text = "2"
            if(aProduct.pair5!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair5
            }
        }else if (indexPath.row == 5) {
            cell.labelTop.text = "3"
            if(aProduct.pair6!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair6
            }
        }else if (indexPath.row == 6) {
            cell.labelTop.text = "4"
            if(aProduct.pair7!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair7
            }
        }else if (indexPath.row == 7) {
            cell.labelTop.text = "5"
            if(aProduct.pair8!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair8
            }
        }else if (indexPath.row == 8) {
            cell.labelTop.text = "6"
            if(aProduct.pair9!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair9
            }
        }else if (indexPath.row == 9) {
            cell.labelTop.text = "7"
            if(aProduct.pair10!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair10
            }
        }else if (indexPath.row == 10) {
            cell.labelTop.text = "8"
            if(aProduct.pair11!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair11
            }
        }else if (indexPath.row == 11) {
            cell.labelTop.text = "9"
            if(aProduct.pair12!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair12
            }
        }

        return cell
    }

}

