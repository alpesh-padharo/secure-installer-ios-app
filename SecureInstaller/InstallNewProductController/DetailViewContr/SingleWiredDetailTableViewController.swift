//
//  SingleWiredDetailTableViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 23/10/20.
//

import UIKit
import SafariServices
import SystemConfiguration


class SingleWiredDetailTableViewController: UITableViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
   

    fileprivate var cellsPerRow:CGFloat = 12
    fileprivate let cellPadding:CGFloat = 2

    var aProduct : Product = Product()
    
    // Cell 1
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelProductStaticName: UILabel!
    @IBOutlet weak var topLabelProductName: UILabel!

    // cell 2
    @IBOutlet weak var labelWiringStaticName: UILabel!
    @IBOutlet weak var labelWiringValue: UILabel!
    
    // cell 3
    @IBOutlet weak var textViewInstructions: UITextView!
    @IBOutlet weak var labelInstructionsStaticName: UILabel!
    
    // Cell 4
    @IBOutlet weak var labelProductname: UILabel!
    @IBOutlet weak var labelWiringDiagramStaticName: UILabel!
    @IBOutlet weak var viewBackground1 : UIView!
    
    // cell 5
    @IBOutlet weak var labeInstallLinklHeader: UILabel!
    @IBOutlet weak var labeInstallLinkFooter: UILabel!
    @IBOutlet weak var buttonInstallLinkbuttonUrl: UIButton!

    @IBOutlet weak var collectionViewControllerWired: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBarModifications()
        
        var lastSearchedArray = DocumentFileManager.shared.getStoredlastSearched()
        if(!lastSearchedArray.contains(String(aProduct.id))){
            lastSearchedArray.append(String(aProduct.id))
            DocumentFileManager.shared.saveLastSearched(array: lastSearchedArray)
        }
        
        if(aProduct.spare_pin != nil) {
            if(aProduct.spare_pin?.count ?? 0 <= 0) {
                cellsPerRow = 10
            }
        } else {
            cellsPerRow = 10
        }
        collectionViewControllerWired.dataSource = self
        collectionViewControllerWired.delegate = self
        collectionViewControllerWired.reloadData()

        
        updateProductNameCell()
        updateWiringAndBackplateCell()
        updateInstructionsCell()
        updateDiagramCell()
        updateInstallLinkCell()
    }
    
    func updateProductNameCell () {
        labelProductStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        topLabelProductName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        topLabelProductName?.text = aProduct.productModelName
        imageViewProduct?.image = UIImage.convertBase64TypeToImage(imgString: aProduct.image!)
    }
    
    func updateWiringAndBackplateCell () {
        labelWiringStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelWiringValue?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelWiringValue.text = aProduct.wiringAndBackPlate
    }
    
    func updateInstructionsCell () {
        labelInstructionsStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        textViewInstructions?.font = UIFont(name: "Kohinoor Devanagari", size: 11)
        let stringArray = aProduct.instructions!.components(separatedBy: "|").filter({ $0 != ""})
        textViewInstructions.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))
    }
    
    func updateDiagramCell () {
        labelWiringDiagramStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelProductname?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        
        labelWiringDiagramStaticName.text = "Wiring Diagram"
        labelProductname.text = "\(aProduct.productModelName ?? "")"
        viewBackground1.layer.borderWidth = 1
        viewBackground1.layer.borderColor = UIColor.gray.cgColor
    }
    
    func updateInstallLinkCell () {
        labeInstallLinklHeader.text = "Follow the link below the guided installation"
        buttonInstallLinkbuttonUrl.addTarget(self, action: #selector(testFunc(button:)), for: .touchUpInside)
        buttonInstallLinkbuttonUrl.setTitle("Install \(aProduct.productModelName ?? "")", for: .normal)
        labeInstallLinkFooter.text = "The link will redirect you to the secure webpage"

    }

    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 17)!]
        self.title = "New Product Installation"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    public func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }

    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func testFunc(button: UIButton){
        showTutorial()
    }
    
    func showTutorial() {
        
        if(isConnectedToNetwork())
        {
            if let url = URL(string:aProduct.faqurl ?? "https://www.securemeters.com") {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true
                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        } else {
            let alert = UIAlertController(title: "Alert", message: "Please check your internet connection.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                  
                /*switch action.style{
                 
                  case .default:
                        print("default")

                  case .cancel:
                        print("cancel")

                  case .destructive:
                        print("destructive")


                @unknown default:
                    print("default")
                }*/
                
            }))
            self.present(alert, animated: true, completion: nil)

        }
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

}

extension SingleWiredDetailTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return 94
        } else if (indexPath.row == 1) {
            if(aProduct.wiringAndBackPlate!.count <= 0) {
                    return 20
                } else {
                    return 60
                }
        }else if (indexPath.row == 2) {
            
            if(aProduct.instructions!.count <= 0) {
                return 37
            } else {
                return 137
            }
            
        }else if (indexPath.row == 3) {
            return 170
        }else if (indexPath.row == 4) {
            return 81
        } else {
            return 44
        }
        
    }
}

extension SingleWiredDetailTableViewController {
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            guard let windowInterfaceOrientation = self.windowInterfaceOrientation else { return }
            
            if windowInterfaceOrientation.isLandscape {
                // activate landscape changes
                
            } else {
                // activate portrait changes
            }
            self.collectionViewControllerWired.collectionViewLayout.invalidateLayout()
        })
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var SpaceNeedToRemove = 32
        if (UIDevice.current.hasNotch) {
            //... consider notch
            if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight) {
                SpaceNeedToRemove = SpaceNeedToRemove + 80
            }
        }
        let widthMinusPadding = UIScreen.main.bounds.width - CGFloat(SpaceNeedToRemove) - (cellPadding + cellPadding * cellsPerRow)
        
        let eachSide = widthMinusPadding / cellsPerRow
        return CGSize(width: eachSide, height: 89)
        
        
    }
    
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            // Fallback on earlier versions
            
            return UIApplication.shared.statusBarOrientation
        }
    }
}

extension SingleWiredDetailTableViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SingleWiredCollectionViewCell", for: indexPath) as! SingleWiredCollectionViewCell
        cell.labelTop.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
        cell.labelBottom.font = UIFont(name: "Kohinoor Devanagari", size: 7)!
        
        cell.labelTop.layer.backgroundColor  = UIColor.gray.cgColor
        cell.labelTop.layer.cornerRadius = 10
        cell.labelTop.layer.masksToBounds = true
        cell.labelTop.backgroundColor = UIColor.white
        
        cell.labelTop.layer.borderWidth = 1
        cell.labelTop.layer.borderColor = UIColor.gray.cgColor


        if(indexPath.row == 0) {
            
            cell.labelTop.text = "E"
            
            if(aProduct.pair1!.count <= 0){
                cell.labelBottom.text = "E"
            }else {
                cell.labelBottom.text = aProduct.pair1
            }
            
            
        } else if (indexPath.row == 1) {
            cell.labelTop.text = "N"
            if(aProduct.pair2!.count <= 0){
                cell.labelBottom.text = "N"
            } else {
                cell.labelBottom.text = aProduct.pair2
            }
            
        }else if (indexPath.row == 2) {
            cell.labelTop.text = "L"
            if(aProduct.pair3!.count <= 0){
                cell.labelBottom.text = "L"
            } else {
                cell.labelBottom.text = aProduct.pair3
            }
            
        }else if (indexPath.row == 3) {
            cell.labelTop.text = "1"
            if(aProduct.pair4!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair4
            }
        }else if (indexPath.row == 4) {
            cell.labelTop.text = "2"
            if(aProduct.pair5!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair5
            }
        }else if (indexPath.row == 5) {
            cell.labelTop.text = "3"
            if(aProduct.pair6!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair6
            }
        }else if (indexPath.row == 6) {
            cell.labelTop.text = "4"
            if(aProduct.pair7!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair7
            }
        }else if (indexPath.row == 7) {
            cell.labelTop.text = "5"
            if(aProduct.pair8!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair8
            }
        }else if (indexPath.row == 8) {
            cell.labelTop.text = "6"
            if(aProduct.pair9!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair9
            }
        }else if (indexPath.row == 9) {
            cell.labelTop.text = "7"
            if(aProduct.pair10!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair10
            }
        }else if (indexPath.row == 10) {
            cell.labelTop.text = "8"
            if(aProduct.pair11!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair11
            }
        }else if (indexPath.row == 11) {
            cell.labelTop.text = "9"
            if(aProduct.pair12!.count <= 0){
                cell.isHidden = true
            } else {
                cell.labelBottom.text = aProduct.pair12
            }
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
}



