//
//  InstallLinkCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit

class InstallLinkCell: UITableViewCell {

    @IBOutlet weak var labelHeader: UILabel!
    @IBOutlet weak var labelFooter: UILabel!
    @IBOutlet weak var buttonUrl: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelHeader?.font = UIFont(name: "Kohinoor Devanagari", size: 11)
        labelFooter?.font = UIFont(name: "Kohinoor Devanagari", size: 11)
        buttonUrl.titleLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 11)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
