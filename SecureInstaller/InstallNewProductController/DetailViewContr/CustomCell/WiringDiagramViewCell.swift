//
//  WiringDiagramViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit

class WiringDiagramViewCell: UITableViewCell {

    @IBOutlet weak var labelProductname: UILabel!
    @IBOutlet weak var labelStaticName: UILabel!
    @IBOutlet weak var collectionViewSingleWired: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelProductname?.font = UIFont(name: "Kohinoor Devanagari", size: 12)

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
