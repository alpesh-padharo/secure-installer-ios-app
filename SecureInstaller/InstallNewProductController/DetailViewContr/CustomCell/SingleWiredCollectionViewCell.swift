//
//  SingleWiredCollectionViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit

class SingleWiredCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelBottom: UILabel!
    @IBOutlet weak var labelTop: UILabel!
}
