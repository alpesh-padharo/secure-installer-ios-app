//
//  InstructionsViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit

class InstructionsViewCell: UITableViewCell {

    @IBOutlet weak var textViewInstructions: UITextView!
    @IBOutlet weak var labelStaticName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        textViewInstructions?.font = UIFont(name: "Kohinoor Devanagari", size: 11)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
