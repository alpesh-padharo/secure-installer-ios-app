//
//  WiringAndBackplateCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//

import UIKit

class WiringAndBackplateCell: UITableViewCell {

    @IBOutlet weak var labelStaticName: UILabel!
    @IBOutlet weak var labelValue: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
     
        // Initialization code
        
        labelStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelValue?.font = UIFont(name: "Kohinoor Devanagari", size: 12)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
