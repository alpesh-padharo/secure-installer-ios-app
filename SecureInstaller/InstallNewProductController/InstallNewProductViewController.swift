//
//  InstallNewProductViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 16/10/20.
//

import UIKit
import SafariServices
import EmptyDataSet_Swift

class InstallNewProductViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    fileprivate var isSearching : Bool = false

    let kHeaderSectionTag: Int = 6900;

    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var tableHeaderLabel: UILabel!
    @IBOutlet weak var tableHeaderSearchTextfield: UITextField!{
        didSet {
            tableHeaderSearchTextfield.tintColor = UIColor.lightGray
            tableHeaderSearchTextfield.setIcon(#imageLiteral(resourceName: "zoom-out"))
         }
    }

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewLastSearched: UICollectionView!

    internal var arrayProduct = [Product]()
    fileprivate var arrayUniqueProduct = [Product]()
    fileprivate var arrayfilteredProductList: [Product] = []
    fileprivate var arrayProductList = [Product]()
    fileprivate var arrayLastSearchedProductList = [Product]()
    
    var expandedSectionHeaderNumber: Int = -1
    var expandedSectionHeader: UITableViewHeaderFooterView!
    var sectionItems: Array<Any> = []
    var sectionNames: Array<Any> = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView!.tableFooterView = UIView()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        collectionView.emptyDataSetSource = self
        collectionView.emptyDataSetDelegate = self

        collectionViewLastSearched.delegate = self
        collectionViewLastSearched.dataSource = self
        
        setupUI()
        navBarModifications()
        setProductDataFromSelectedProduct()
        
    }
    
    private func setProductDataFromSelectedProduct() {
        
        arrayProductList = arrayProduct
        arrayUniqueProduct = arrayProductList.unique{$0.productModelName}
        arrayUniqueProduct = arrayProductList.filter { $0.manufacturerName!.contains("Secure") }

        sectionNames = ["Heating and Hot water controls"]
        
        sectionItems = [ ["TimeSwitches & programmers",
                          "Programmable thermostat",
                          "Motorised valves & actuators",
                          "Electric water heating",
                          "Thermostat"]
                        ];
        
        arrayLastSearchedProductList = DocumentFileManager.shared.prePareProductListOfLastSearched(productArray: arrayProduct)
        collectionViewLastSearched.reloadData()

        collectionView.reloadData()
        tableView.reloadData()
        
    }

    
    fileprivate func setupUI() {
        tableHeaderLabel.font = UIFont(name: "Kohinoor Devanagari", size: 14)
        tableHeaderLabel.text = "Secure Product"
        tableHeaderSearchTextfield.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        tableHeaderSearchTextfield.layer.borderWidth = 1
        tableHeaderSearchTextfield.layer.borderColor = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 241.0 / 255.0, alpha: 1).cgColor
        tableHeaderSearchTextfield.backgroundColor = .clear
        tableHeaderSearchTextfield.leftViewMode = .always
        tableHeaderSearchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
        // tableHeaderSearchTextfield.delegate = self
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if(textField.text?.count ?? 0 > 0) {
            isSearching = true
        } else {
            isSearching = false
        }
        arrayfilteredProductList = arrayUniqueProduct.filter { $0.productModelName!.localizedCaseInsensitiveContains(textField.text!) }
        collectionView.reloadData()
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 17)!]
        self.title = "New Product Installation"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if sectionNames.count > 0 {
            tableView.backgroundView = nil
            return sectionNames.count
        } else {
//            let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.size.width, height: view.bounds.size.height))
//            messageLabel.text = "Retrieving data.\nPlease wait."
//            messageLabel.numberOfLines = 0;
//            messageLabel.textAlignment = .center;
//            messageLabel.font = UIFont(name: "HelveticaNeue", size: 20.0)!
//            messageLabel.sizeToFit()
//            self.tableView.backgroundView = messageLabel;
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (self.expandedSectionHeaderNumber == section) {
            let arrayOfItems = self.sectionItems[section] as! NSArray
            return arrayOfItems.count;
        } else {
            return 0;
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (self.sectionNames.count != 0) {
            return self.sectionNames[section] as? String
        }
        return ""
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0;
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat{
        return 0;
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        //recast your view as a UITableViewHeaderFooterView
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.contentView.backgroundColor = hexStringToUIColor(hex: "#79265C")
        header.textLabel?.textColor = UIColor.white
        
        if let viewWithTag = self.view.viewWithTag(kHeaderSectionTag + section) {
            viewWithTag.removeFromSuperview()
        }
        let headerFrame = self.view.frame.size
        let theImageView = UIImageView(frame: CGRect(x: headerFrame.width - 32, y: 13, width: 18, height: 18));
        theImageView.image = UIImage(named: "Chevron-Dn-Wht")
        theImageView.tag = kHeaderSectionTag + section
        header.addSubview(theImageView)
        
        // make headers touchable
        header.tag = section
        let headerTapGesture = UITapGestureRecognizer()
        headerTapGesture.addTarget(self, action: #selector(ProductGuideViewController.sectionHeaderWasTouched(_:)))
        header.addGestureRecognizer(headerTapGesture)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as UITableViewCell
        let section = self.sectionItems[indexPath.section] as! NSArray
        cell.textLabel?.textColor = UIColor.black
        cell.textLabel?.text = section[indexPath.row] as? String
        cell.textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 14)!

        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = self.sectionItems[indexPath.section] as! NSArray
        let selectedCategory = section[indexPath.row] as? String
        if(!selectedCategory!.isEmpty)
        {
            navigateToProductList(selectedRecord: selectedCategory!)
        }
        tableView.deselectRow(at: indexPath, animated: true)

    }
    
    func navigateToProductList (selectedRecord : String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let productListViewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        productListViewController.selectedSubcategoryName = selectedRecord
        productListViewController.arrayProduct = arrayProduct
        productListViewController.isMovingFrominstallNewProductScreen = true
        self.navigationController?.pushViewController(productListViewController, animated: true)
    }
    
    // MARK: - Expand / Collapse Methods
    
    @objc func sectionHeaderWasTouched(_ sender: UITapGestureRecognizer) {
        let headerView = sender.view as! UITableViewHeaderFooterView
        let section    = headerView.tag
        let eImageView = headerView.viewWithTag(kHeaderSectionTag + section) as! UIImageView?
        
        if (self.expandedSectionHeaderNumber == -1) {
            self.expandedSectionHeaderNumber = section
            tableViewExpandSection(section, imageView: eImageView!)
        } else {
            if (self.expandedSectionHeaderNumber == section) {
                tableViewCollapeSection(section, imageView: eImageView!)
            } else {
                let cImageView = self.view.viewWithTag(kHeaderSectionTag + self.expandedSectionHeaderNumber) as! UIImageView?
                tableViewCollapeSection(self.expandedSectionHeaderNumber, imageView: cImageView!)
                tableViewExpandSection(section, imageView: eImageView!)
            }
        }
    }
    
    func tableViewCollapeSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        self.expandedSectionHeaderNumber = -1;
        if (sectionData.count == 0) {
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (0.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.tableView!.beginUpdates()
            self.tableView!.deleteRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
    
    func tableViewExpandSection(_ section: Int, imageView: UIImageView) {
        let sectionData = self.sectionItems[section] as! NSArray
        
        if (sectionData.count == 0) {
            self.expandedSectionHeaderNumber = -1;
            return;
        } else {
            UIView.animate(withDuration: 0.4, animations: {
                imageView.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)
            })
            var indexesPath = [IndexPath]()
            for i in 0 ..< sectionData.count {
                let index = IndexPath(row: i, section: section)
                indexesPath.append(index)
            }
            self.expandedSectionHeaderNumber = section
            self.tableView!.beginUpdates()
            self.tableView!.insertRows(at: indexesPath, with: UITableView.RowAnimation.fade)
            self.tableView!.endUpdates()
        }
    }
}

extension InstallNewProductViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView == collectionViewLastSearched) {
            return arrayLastSearchedProductList.count
        } else {
            if (isSearching == false ){
                return arrayUniqueProduct.count
            } else {
                return arrayfilteredProductList.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView == collectionViewLastSearched) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchProductCollectionViewCell", for: indexPath) as! SearchProductCollectionViewCell

            cell.labelName.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
                cell.labelName.text = arrayLastSearchedProductList[indexPath.row].productModelName
            cell.imageView.image = UIImage.convertBase64TypeToImage(imgString: arrayLastSearchedProductList[indexPath.row].image!)
            cell.borderView.roundedView()

            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewProductCollectionViewCell", for: indexPath) as! NewProductCollectionViewCell
            cell.labelName.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
            cell.labelIsNew.font = UIFont(name: "Kohinoor Devanagari", size: 8)!
            cell.labelIsNew.layer.cornerRadius = 5.0
            cell.labelIsNew.clipsToBounds = true
            cell.borderView.roundedView()
            
            if arrayfilteredProductList.count == 0 {
                cell.labelName.text = arrayUniqueProduct[indexPath.row].productModelName
                cell.imageView?.image = UIImage.convertBase64TypeToImage(imgString: arrayUniqueProduct[indexPath.row].image!)
                if(arrayUniqueProduct[indexPath.row].isNewInstall == "Yes") {
                    cell.labelIsNew.isHidden = false
                } else {
                    cell.labelIsNew.isHidden = true
                }
            } else {
                cell.labelName.text = arrayfilteredProductList[indexPath.row].productModelName
                cell.imageView.image = UIImage.convertBase64TypeToImage(imgString: arrayfilteredProductList[indexPath.row].image!)
                
                if(arrayfilteredProductList[indexPath.row].isNewInstall == "Yes") {
                    cell.labelIsNew.isHidden = false
                } else {
                    cell.labelIsNew.isHidden = true
                }
            }
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if(collectionView == collectionViewLastSearched) {
            navigateToSingleWiredDetailView(selectedRecord: arrayLastSearchedProductList[indexPath.row])
        } else {
            if arrayfilteredProductList.count == 0 {
                navigateToSingleWiredDetailView(selectedRecord: arrayUniqueProduct[indexPath.row])
            } else {
                navigateToSingleWiredDetailView(selectedRecord: arrayfilteredProductList[indexPath.row])
            }
        }
        
    }
    func navigateToSingleWiredDetailView (selectedRecord : Product) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let singleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "SingleWiredDetailViewController") as! SingleWiredDetailViewController
//        singleWiredDetailtViewController.aProduct = selectedRecord
//        self.navigationController?.pushViewController(singleWiredDetailtViewController, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let singleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "SingleWiredDetailTableViewController") as! SingleWiredDetailTableViewController
        singleWiredDetailtViewController.aProduct = selectedRecord
        self.navigationController?.pushViewController(singleWiredDetailtViewController, animated: true)

    }
}

extension InstallNewProductViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 91, height: 85)
    }
    
}

extension InstallNewProductViewController : EmptyDataSetSource, EmptyDataSetDelegate {
    
    //MARK: - DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No data found", attributes: [NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 19) as Any])
    }

    //MARK: - DZNEmptyDataSetDelegate Methods
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }

}

extension UIImage {
    var circleMask: UIImage {
        let square = size.width < size.height ? CGSize(width: size.width, height: size.width) : CGSize(width: size.height, height: size.height)
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: square))
        imageView.contentMode = UIView.ContentMode.scaleAspectFill
        imageView.image = self
        imageView.layer.cornerRadius = square.width/2
        imageView.layer.borderColor = UIColor.lightGray.cgColor
        imageView.layer.borderWidth = 5
        imageView.layer.masksToBounds = true
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
}

extension UIView {
    func roundedView() {
        self.layer.cornerRadius = (self.frame.size.width) / 2;
        self.clipsToBounds = true
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.darkGray.cgColor
    }
}
