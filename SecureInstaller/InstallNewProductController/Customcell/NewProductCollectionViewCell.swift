//
//  NewProductCollectionViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 16/10/20.
//

import UIKit

class NewProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelIsNew: UILabel!
    @IBOutlet weak var borderView: UIView!


}
