//
//  ProductAPI.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 12/10/20.
//

import Foundation
import CoreData

final class ProductAPI {
    // 1
    static let shared = ProductAPI()
    // 2
    private init() {
        persistencyManager = PersistencyManager(modelName: "SecureInstaller", completion: {
        })
    }
    
    private var persistencyManager: PersistencyManager?
    
    // private let persistencyManager = PersistencyManager.sharedManager
    private let apiManager = APIClient.sharedManager
    private let isOnline = false
    
    public func copyBundleDataToLocalAppDirectory() {
        return persistencyManager!.copyBundleDataToLocalAppDirectory()
    }
    
    public func saveProductDataInLocalStorage() {
        return persistencyManager!.importJSONData()
    }
    
    public func resetAllRecords()
    {
        return persistencyManager!.resetAllRecords(in: "Product")
    }
    
    public func getLocalDatbaseVersionNumber() -> Int {
        return persistencyManager!.getLocalDatabaseVersion()
    }
    
    public func saveDataToLocalStorage(json : Data) {
        return persistencyManager!.saveDataToLocalStorage(jsonData: json)
    }
    
    public func getAllProducts() -> [Product]{
        return persistencyManager!.fetchAllProducts() ?? []
    }
        
    public func getAuthToken(withComplition authCompletionHandler : @escaping (AuthResponse)->() ) {
        apiManager.getAuthToken { (authresponse) in
            authCompletionHandler (authresponse)
        }
    }
    
    public func getProducts (withToken strToken:String, withComplition productCompletionHandler : @escaping (ProductApiResponse)->() ) {
        
        // use token
        apiManager.getProductsData(withToken: strToken) { (aProductResponse) in
            productCompletionHandler(aProductResponse)
        }
    }

}
