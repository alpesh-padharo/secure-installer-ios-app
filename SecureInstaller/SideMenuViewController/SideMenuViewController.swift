//
//  SideMenuViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 14/10/20.
//

import UIKit
import SideMenu

class SideMenuViewController: UIViewController {

    fileprivate enum Constants {
      static let MenuCellIdentifier = "MenuCell"
    }
    
    fileprivate let sideMenuItems = ["FAQ","Video","Contact US"]
        
    @IBOutlet weak var tableMenu: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        guard let menu = navigationController as? SideMenuNavigationController, menu.blurEffectStyle == nil else {
            return
        }
        
        tableMenu.delegate = self
        tableMenu.dataSource = self
        tableMenu.tableFooterView = UIView()

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SideMenuViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0) {
            
        } else if(indexPath.row == 1) {
            
        } else if(indexPath.row == 2) {
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
}

extension SideMenuViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MenuCellIdentifier, for: indexPath)
        cell.textLabel?.text = sideMenuItems[indexPath.row]
        cell.textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        return cell

    }
    
    
}
