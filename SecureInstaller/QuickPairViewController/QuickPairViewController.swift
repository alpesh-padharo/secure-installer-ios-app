//
//  QuickPairViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 15/10/20.
//

import UIKit
import WebKit

class QuickPairViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationBarModifications()
        // Do any additional setup after loading the view.
        


    }
    
    func navigationBarModifications () {
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "#79265C")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]

        self.navigationController?.navigationBar.titleTextAttributes =
                  [NSAttributedString.Key.foregroundColor: UIColor.white,
                   NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
         self.title = "Quick Pair"
        
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
