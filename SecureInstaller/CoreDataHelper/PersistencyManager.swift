//
//  PersistencyManager.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 12/10/20.
//

import Foundation
import CoreData
import UIKit

class PersistencyManager {
    
    // MARK: - Type Aliases

    public typealias CoreDataManagerCompletion = () -> ()

    // MARK: - Properties

    private let modelName: String

    // MARK: -

    private let completion: CoreDataManagerCompletion

    // MARK: - Initialization

    public init(modelName: String, completion: @escaping CoreDataManagerCompletion) {
        // Set Properties
        self.modelName = modelName
        self.completion = completion

        // Setup Core Data Stack
        setupCoreDataStack()
    }

    
    private func setupCoreDataStack() {
        // Fetch Persistent Store Coordinator
        guard let persistentStoreCoordinator = persistentContainer.viewContext.persistentStoreCoordinator else {
            fatalError("Unable to Set Up Core Data Stack")
        }

        DispatchQueue.global().async {
            // Add Persistent Store
            self.addPersistentStore(to: persistentStoreCoordinator)

            // Invoke Completion On Main Queue
            DispatchQueue.main.async { self.completion() }
        }
    }
    
    private func addPersistentStore(to persistentStoreCoordinator: NSPersistentStoreCoordinator) {
        // Helpers
        let fileManager = FileManager.default
        let storeName = "\(self.modelName).sqlite"

        // URL Documents Directory
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

        // URL Persistent Store
        let persistentStoreURL = documentsDirectoryURL.appendingPathComponent(storeName)

        do {
            let options = [
                NSMigratePersistentStoresAutomaticallyOption : true,
                NSInferMappingModelAutomaticallyOption : true,
                NSPersistentStoreFileProtectionKey : FileProtectionType.complete
            ] as [String : Any]

            // Add Persistent Store
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: options)

        } catch {
            fatalError("Unable to Add Persistent Store")
        }
    }

    

    
    //static let sharedManager = PersistencyManager()
    
    // private init() {}
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "SecureInstaller")
        
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func delete(product : Product) {
        let managedContext = persistentContainer.viewContext
        
        do {
            managedContext.delete(product)
        }
        
        do {
            try managedContext.save()
        }
        catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }
    
    func resetAllRecords(in entity : String)
    {
        let managedContext = persistentContainer.viewContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do
        {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        }
        catch
        {
            print ("There was an error")
        }
    }
    
    
    func fetchAllProducts() -> [Product]? {
        let managedContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Product")
        
        do {
            let items = try managedContext.fetch(fetchRequest)
            return exportToProductObject(items)
        }
        catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            return nil
        }
    }
    
    
    public func copyBundleDataToLocalAppDirectory() {

        // Get Data from main bundle
        let jsonURL = Bundle.main.url(forResource: "databasefile", withExtension: "json")!
        let jsonData = try! Data(contentsOf: jsonURL)
        
        do {
            let jsonURL = DocumentFileManager.shared.getLocalDatabaseJsonFilePath()
            try jsonData.write(to: jsonURL)
            
            let defaults = UserDefaults.standard
            defaults.set(true, forKey: "isDataCopiedInLocalAppDirectory")

        }catch {
            print(error)
        }

    }
    
    public func saveDataToLocalStorage(jsonData : Data) {
        do {
            
           

            let jsonURL = DocumentFileManager.shared.getLocalDatabaseJsonFilePath()
            try jsonData.write(to: jsonURL)
            
        }catch {
            print(error)
        }
    }
    
    func getLocalDatabaseVersion() -> Int {
        let jsonURL = DocumentFileManager.shared.getLocalDatabaseJsonFilePath()
        let jsonData = try! Data(contentsOf: jsonURL)
        let jsonDict = try! JSONSerialization.jsonObject(with: jsonData, options: [.allowFragments]) as! [String: Any]
        let version = jsonDict["dbTableVersion"] as! Int
        return version
    }
    
    func exportToProductObject(_ items : [Any]) -> [Product] {
        var resultArray = [Product]()
        for item in items {
            let product = item as? Product
            if product == nil {
                continue
            }
            if product!.manufacturerName != nil && product!.manufacturerName?.base64Decoded() == nil {
                resultArray.append(item as! Product)
                continue
            }
            product?.manufacturerName = product!.manufacturerName == nil ? "" : product!.manufacturerName?.base64Decoded()
            product?.productModelName = product!.productModelName?.base64Decoded()
            product?.pair1 = product!.pair1?.base64Decoded()
            product?.pair2 = product!.pair2?.base64Decoded()
            product?.pair3 = product!.pair3?.base64Decoded()
            product?.pair4 = product!.pair4?.base64Decoded()
            product?.pair5 = product!.pair5?.base64Decoded()
            product?.pair6 = product!.pair6?.base64Decoded()
            product?.pair7 = product!.pair7?.base64Decoded()
            product?.pair8 = product!.pair8?.base64Decoded()
            product?.pair9 = product!.pair9?.base64Decoded()
            product?.pair10 = product!.pair10?.base64Decoded()
            product?.pair11 = product!.pair11?.base64Decoded()
            product?.pair12 = product!.pair12?.base64Decoded()
            product?.spare_pin = product!.spare_pin?.base64Decoded()
            product?.otherCompatibleProductName = product!.otherCompatibleProductName?.base64Decoded()
            product?.recommendProductName = product!.recommendProductName?.base64Decoded()
            product?.recommendProductInstructions = product!.recommendProductInstructions?.base64Decoded()
            product?.productCategory = product!.productCategory?.base64Decoded()
            product?.productSubCategory = product!.productSubCategory?.base64Decoded()
            product?.modelImagePath = product!.modelImagePath?.base64Decoded()
            product?.image = product!.image?.base64Decoded()
            product?.applicationDetails = product!.applicationDetails?.base64Decoded()
            product?.wiringAndBackPlate = product!.wiringAndBackPlate?.base64Decoded()
            product?.instructions = product!.instructions?.base64Decoded()
            product?.isNewInstall = product!.isNewInstall?.base64Decoded()
            product?.isCommissioningProduct = product!.isCommissioningProduct?.base64Decoded()
            product?.faqurl = product!.faqurl?.base64Decoded()
            product?.videoURL = product!.videoURL?.base64Decoded()
            product?.otherUrl = product!.otherUrl?.base64Decoded()
            resultArray.append(product!)
        }
        return resultArray
    }
    
    
    func importJSONData() {
        
        let managedContext = persistentContainer.viewContext
        
        
        let jsonURL = DocumentFileManager.shared.getLocalDatabaseJsonFilePath()
        let jsonData = try! Data(contentsOf: jsonURL)
        
        let jsonDict = try! JSONSerialization.jsonObject(with: jsonData, options: [.allowFragments]) as! [String: Any]
        let respArray = jsonDict["responseObj"]
        //print(respArray.debugDescription)
        let responseArray = (respArray as! NSArray) as Array
        // print(responseArray)
        
        for productDict in responseArray {
            let manufacturerName = productDict["manufacturerName"] as? String
            let productModelName = productDict["productModelName"] as? String
            let pair1 = productDict["pair1"] as? String
            let pair2 = productDict["pair2"] as? String
            let pair3 = productDict["pair3"] as? String
            let pair4 = productDict["pair4"] as? String
            let pair5 = productDict["pair5"] as? String
            let pair6 = productDict["pair6"] as? String
            let pair7 = productDict["pair7"] as? String
            let pair8 = productDict["pair8"] as? String
            let pair9 = productDict["pair9"] as? String
            let pair10 = productDict["pair10"] as? String
            let pair11 = productDict["pair11"] as? String
            let pair12 = productDict["pair12"] as? String
            let spare_pin = productDict["spare_Pin"] as? String
            let otherCompatibleProductName = productDict["otherCompatibleProductName"] as? String
            let recommendProductName = productDict["recommendProductName"] as? String
            let recommendProductInstructions = productDict["recommendProductInstructions"] as? String
            let productCategory = productDict["productCategory"] as? String
            let productSubCategory = productDict["productSubCategory"] as? String
            let modelImagePath = productDict["modelImagePath"] as? String
            let image = productDict["image"] as? String
            let applicationDetails = productDict["applicationDetails"] as? String
            let wiringAndBackPlate = productDict["wiringAndBackPlate"] as? String
            let instructions = productDict["instructions"] as? String
            let isNewInstall = productDict["isNewInstall"] as? String
            let isCommissioningProduct = productDict["isCommissioningProduct"] as? String
            let faqurl = productDict["faqurl"] as? String
            let videoURL = productDict["videoURL"] as? String
            let otherUrl = productDict["otherUrl"] as? String
            let isActive = productDict["isActive"] as? Bool
            let version = productDict["version"] as! Float
            let id = productDict["id"] as! Int64
            let createDate = productDict["createDate"] as? Date
            let updateDate = productDict["updateDate"] as? Date
            
            let products = Product(context: persistentContainer.viewContext)
            products.manufacturerName = manufacturerName?.base64Encoded()
            products.productModelName = productModelName?.base64Encoded()
            products.pair1 = pair1?.base64Encoded()
            products.pair2 = pair2?.base64Encoded()
            products.pair3 = pair3?.base64Encoded()
            products.pair4 = pair4?.base64Encoded()
            products.pair5 = pair5?.base64Encoded()
            products.pair6 = pair6?.base64Encoded()
            products.pair7 = pair7?.base64Encoded()
            products.pair8 = pair8?.base64Encoded()
            products.pair9 = pair9?.base64Encoded()
            products.pair10 = pair10?.base64Encoded()
            products.pair11 = pair11?.base64Encoded()
            products.pair12 = pair12?.base64Encoded()
            products.spare_pin = spare_pin?.base64Encoded()
            products.otherCompatibleProductName = otherCompatibleProductName?.base64Encoded()
            products.recommendProductName = recommendProductName?.base64Encoded()
            products.recommendProductInstructions = recommendProductInstructions?.base64Encoded()
            products.productCategory = productCategory?.base64Encoded()
            products.productSubCategory = productSubCategory?.base64Encoded()
            products.modelImagePath = modelImagePath?.base64Encoded()
            products.image = image?.base64Encoded()
            products.applicationDetails = applicationDetails?.base64Encoded()
            products.wiringAndBackPlate = wiringAndBackPlate?.base64Encoded()
            products.instructions = instructions?.base64Encoded()
            products.isNewInstall = isNewInstall?.base64Encoded()
            products.isCommissioningProduct = isCommissioningProduct?.base64Encoded()
            products.faqurl = faqurl?.base64Encoded()
            products.videoURL = videoURL?.base64Encoded()
            products.otherUrl = otherUrl?.base64Encoded()
            products.isActive = isActive!
            products.version = version
            products.id = id
            products.createDate = createDate
            products.updateDate = updateDate
            
        }
        
        do {
            try managedContext.save()
        }
        catch let error as NSError {
            fatalError("Unresolved error \(error), \(error.userInfo)")
        }
    }
    
}
