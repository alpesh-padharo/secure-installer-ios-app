//
//  ProductListViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 15/10/20.
//

import UIKit
import KRProgressHUD
import SafariServices
import EmptyDataSet_Swift

class ProductListViewController: UIViewController {
    
    var selectedProduct : Product = Product()
    
    var isMovingFromProductGuideScreen : Bool = false
    var isMovingFrominstallNewProductScreen : Bool = false
    var selectedSubcategoryName : String = ""
    
    fileprivate var isSearching : Bool = false

    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var tableHeaderLabel: UILabel!
    @IBOutlet weak var tableHeaderSearchTextfield: UITextField!{
        didSet {
            tableHeaderSearchTextfield.tintColor = UIColor.lightGray
            tableHeaderSearchTextfield.setIcon(#imageLiteral(resourceName: "zoom-out"))
         }
    }
    
    @IBOutlet weak var tableProductList: UITableView!
    
    internal var arrayProduct = [Product]()
    fileprivate var arrayUniqueProduct = [Product]()
    fileprivate var arrayfilteredProductList: [Product] = []
    fileprivate var arrayProductList = [Product]()
    
    
    fileprivate var filteredProductList = [[String : Any]]()
    fileprivate var arrProductList = [[String : Any]]()
    fileprivate var arrUniqueProductList = [[String : Any]]()

    
    fileprivate enum Constants {
      static let ProductListCellIdentifier = "ProductListCell"
    }


    override func viewDidLoad() {
        super.viewDidLoad()

        tableProductList.delegate = self
        tableProductList.dataSource = self
        
        tableProductList.emptyDataSetSource = self
        tableProductList.emptyDataSetDelegate = self

        tableProductList.tableFooterView = UIView()

        navBarModifications()
        setupUI()
        
        if(isMovingFromProductGuideScreen) {
            tableHeaderLabel.text = "Select \(selectedSubcategoryName)"
            setProductDataFromSelectedSubCategory(category: selectedSubcategoryName)
        } else if (isMovingFrominstallNewProductScreen) {
            tableHeaderLabel.text = "Select \(selectedSubcategoryName)"
            setProductDataFromSelectedSubCategoryFromNewInstall(category: selectedSubcategoryName)
        }else {
            setProductDataFromSelectedProduct(aProduct: selectedProduct)
            tableHeaderLabel.text = "Select existing \(selectedProduct.manufacturerName!) product"
        }
        
        // Do any additional setup after loading the view.
    }
    
    private func setProductDataFromSelectedSubCategoryFromNewInstall(category: String) {
        
        arrayProductList = arrayProduct.filter{($0.productSubCategory?.localizedCaseInsensitiveContains(category))!}
        arrayUniqueProduct = arrayProductList.unique{$0.productModelName}
        tableProductList.reloadData()
    }
    
    private func setProductDataFromSelectedSubCategory(category: String) {
        
        arrayProductList = arrayProduct.filter{($0.productSubCategory?.localizedCaseInsensitiveContains(category))!}
        arrayUniqueProduct = arrayProductList.unique{$0.productModelName}
        tableProductList.reloadData()
    }
    private func setProductDataFromSelectedProduct(aProduct: Product) {
        
        arrayProductList = arrayProduct.filter{($0.manufacturerName?.localizedCaseInsensitiveContains(aProduct.manufacturerName!))!}
        arrayUniqueProduct = arrayProductList.unique{$0.productModelName}
        tableProductList.reloadData()
    }
    
    private func setProductData(manufacturerName: String) {
        
        KRProgressHUD.show()
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let products = ProductAPI.shared.getAllProducts()
            
            for product in products {
                if manufacturerName == product.value(forKey: "manufacturerName") as! String {
                    var productDict = [String : Any]()
                    productDict["productModelName"] = product.value(forKey: "productModelName") as! String
                    productDict["image"] = product.value(forKey: "image") as? String
                    self?.arrProductList.append(productDict)
                } else {
                    
                }
            }

          // 2
          DispatchQueue.main.async { [weak self] in
            // 3
            self?.arrUniqueProductList = self?.removeDuplicates(self?.arrProductList ?? []) ?? [[String: Any]]()
            KRProgressHUD.dismiss()
            self?.tableProductList.reloadData()
          }
        }
    }
    
    @objc func removeDuplicates(_ arrayOfDicts: [[String: Any]]) -> [[String: Any]] {
        var noDuplicates = [[String: Any]]()
        var usedNames = [String]()
        for dict in arrayOfDicts {
            if let name = dict["productModelName"], !usedNames.contains(name as! String) {
                noDuplicates.append(dict)
                usedNames.append(name as! String)
            }
        }
        return noDuplicates
    }

    
    fileprivate func setupUI() {
        tableHeaderLabel.font = UIFont(name: "Kohinoor Devanagari", size: 14)
        tableHeaderSearchTextfield.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        tableHeaderSearchTextfield.layer.borderWidth = 1
        tableHeaderSearchTextfield.layer.borderColor = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 241.0 / 255.0, alpha: 1).cgColor
        tableHeaderSearchTextfield.backgroundColor = .clear
        tableHeaderSearchTextfield.leftViewMode = .always
        tableHeaderSearchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
        tableHeaderSearchTextfield.delegate = self
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        if(textField.text?.count ?? 0 > 0) {
            isSearching = true
        } else {
            isSearching = false
        }
        
        arrayfilteredProductList = arrayUniqueProduct.filter { $0.productModelName!.localizedCaseInsensitiveContains(textField.text!) }
        tableProductList.reloadData()
    }
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
        
        if(isMovingFromProductGuideScreen) {
            self.title = "Product guide"
        } else if(isMovingFrominstallNewProductScreen) {
            self.title = "New Product Installation"
        } else {
            self.title = "Replace Programmer"
        }
        
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UIImage {
    class func convertBase64TypeToImage(imgString: String) -> UIImage {
         var image: UIImage?
         if let decodedData = Data(base64Encoded: imgString, options: .ignoreUnknownCharacters) {
             image = UIImage(data: decodedData)
         }
         return image ?? UIImage()
     }
}

extension ProductListViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension ProductListViewController: UITableViewDataSource, UITableViewDelegate {
    
    // MARK: TableView methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (isSearching == false) {
            return arrayUniqueProduct.count
        } else {
            return arrayfilteredProductList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ProductListCellIdentifier) as! ProductListCell?
        
        if  arrayfilteredProductList.count == 0 {
            cell?.labelTitle.text = arrayUniqueProduct[indexPath.row].productModelName!
            cell?.imageProduct.image = UIImage.convertBase64TypeToImage(imgString: arrayUniqueProduct[indexPath.row].image!)
        } else {
            cell?.labelTitle.text = arrayfilteredProductList[indexPath.row].productModelName!
            cell?.imageProduct.image = UIImage.convertBase64TypeToImage(imgString: arrayfilteredProductList[indexPath.row].image!)
        }
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if(isMovingFromProductGuideScreen) {
            showTutorial(indexPath.row)
        }else if(isMovingFrominstallNewProductScreen) {
            
            if arrayfilteredProductList.count == 0 {
                navigateToSingleWiredDetailView(selectedRecord:arrayUniqueProduct[indexPath.row])
            } else {
                navigateToSingleWiredDetailView(selectedRecord:arrayfilteredProductList[indexPath.row])
            }
            
        }else {
            if arrayfilteredProductList.count == 0 {
                navigateToReplacementProductController(selectedRecord: arrayUniqueProduct[indexPath.row])
            } else {
                navigateToReplacementProductController(selectedRecord: arrayfilteredProductList[indexPath.row])
            }
        }
        
    }
    func navigateToReplacementProductController (selectedRecord : Product) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let replacementProductController = storyBoard.instantiateViewController(withIdentifier: "ReplacementProductViewController") as! ReplacementProductViewController
        replacementProductController.selectedProduct = selectedRecord
        replacementProductController.arrayProduct = arrayProduct
        self.navigationController?.pushViewController(replacementProductController, animated: true)
    }
    
    func navigateToSingleWiredDetailView (selectedRecord : Product) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let singleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "SingleWiredDetailViewController") as! SingleWiredDetailViewController
//        singleWiredDetailtViewController.aProduct = selectedRecord
//        self.navigationController?.pushViewController(singleWiredDetailtViewController, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let singleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "SingleWiredDetailTableViewController") as! SingleWiredDetailTableViewController
        singleWiredDetailtViewController.aProduct = selectedRecord
        self.navigationController?.pushViewController(singleWiredDetailtViewController, animated: true)

    }
    
    func showTutorial(_ which: Int) {
        
        var web_link = "";
        if arrayfilteredProductList.count == 0 {
            web_link = arrayUniqueProduct[which].faqurl ?? "https://www.securemeters.com"
        } else {
            web_link = arrayfilteredProductList[which].faqurl ?? "https://www.securemeters.com"
        }
        
        if let url = URL(string : web_link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }

    
}

extension ProductListViewController : EmptyDataSetSource, EmptyDataSetDelegate {
    
    //MARK: - DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No data found", attributes: [NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 19) as Any])
    }

    //MARK: - DZNEmptyDataSetDelegate Methods
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }

}
