//
//  ProductListCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 15/10/20.
//

import UIKit

class ProductListCell: UITableViewCell {

    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelTitle?.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        labelTitle?.textColor = UIColor(red: 96.0 / 255.0, green: 96.0 / 255.0, blue: 96.0 / 255.0, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
