//
//  ReplacementProductViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit

class ReplacementProductViewController: UIViewController {
    
    internal var arrayProduct = [Product]()
    internal var selectedProduct : Product = Product()
    
    internal var arrayReplacementProduct : [Product] = [Product]()
    internal var arrayCompatibleProduct : [Product] = [Product]()
    
    var expandedSectionOneIndexSet : IndexSet = []
    var expandedSectionTwoIndexSet : IndexSet = []
    
    @IBOutlet weak var tableviewReplacement: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableviewReplacement.dataSource = self
        tableviewReplacement.delegate = self
        tableviewReplacement.tableFooterView = UIView()
        
        navBarModifications()
        prepareReplacementAndCompaitbaleProduct(selectedRecord: selectedProduct)
    }
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
        self.title = "Replace Programmer"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
        backBarButton.tintColor = .white
        self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func prepareReplacementAndCompaitbaleProduct (selectedRecord : Product){
        var arrayOfSameProductModelname : [Product] = [Product]()
        arrayOfSameProductModelname  = arrayProduct.filter { $0.productModelName!.contains(selectedRecord.productModelName!) }
        
        var replacementProductarray = [String]()
        var CompaitbaleProduct = [String]()
        
        arrayOfSameProductModelname.forEach { (aProduct) in
            let aLocalArray1 =  (aProduct.recommendProductName?.components(separatedBy: ","))!
            let aLocalArray2 =  (aProduct.otherCompatibleProductName?.components(separatedBy: ","))!
            
            replacementProductarray.append(contentsOf: aLocalArray1)
            CompaitbaleProduct.append(contentsOf: aLocalArray2)
        }
        
        replacementProductarray = replacementProductarray.filter({$0 != ""})
        replacementProductarray = replacementProductarray.unique(by: {$0})
        
        CompaitbaleProduct = CompaitbaleProduct.filter({$0 != ""})
        CompaitbaleProduct = CompaitbaleProduct.unique(by: {$0})
        
        print(replacementProductarray)
        print(CompaitbaleProduct)
        
        arrayReplacementProduct = prePareProductListOfLastSearched(productArray: arrayProduct, from: replacementProductarray)
        arrayCompatibleProduct = prePareProductListOfLastSearched(productArray: arrayProduct, from: CompaitbaleProduct)
        
        tableviewReplacement.reloadData()
    }
    
    func prePareProductListOfLastSearched(productArray : [Product], from stringArray:[String]) -> [Product] {
        var arrayTempProduct : [Product] = [Product]()
        let lastSearchedArray = stringArray
        
        //  arrayTempProduct = productArray.filter({lastSearchedArray.contains($0.productModelName!)})
        
        lastSearchedArray.forEach { (item) in
            let trimmed = item.trimmingCharacters(in: .whitespacesAndNewlines)
            print(trimmed)
            let filterdArray : [Product] = productArray.filter { $0.productModelName!.contains(trimmed)}
            arrayTempProduct.append( contentsOf: filterdArray)
        }
        return arrayTempProduct
    }
    
    @objc func testFunc(button: UIButton){
        let section = Int(button.accessibilityIdentifier!)
        if(section == 0) {
            
            if(expandedSectionOneIndexSet.contains(button.tag)){
                expandedSectionOneIndexSet.remove(button.tag)
            } else {
                // if the cell is not expanded, add it to the indexset to expand it
                expandedSectionOneIndexSet.insert(button.tag)
            }
            let indexPath = IndexPath(row: button.tag, section:section!)
            tableviewReplacement.reloadRows(at: [indexPath], with: .automatic)
        } else {
            if(expandedSectionTwoIndexSet.contains(button.tag)){
                expandedSectionTwoIndexSet.remove(button.tag)
            } else {
                // if the cell is not expanded, add it to the indexset to expand it
                expandedSectionTwoIndexSet.insert(button.tag)
            }
            
            let indexPath = IndexPath(row: button.tag, section:section!)
            tableviewReplacement.reloadRows(at: [indexPath], with: .automatic)
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ReplacementProductViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Recommended"
        } else {
            return "Other compatible product"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return arrayReplacementProduct.count
        } else {
            return arrayCompatibleProduct.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // if cant dequeue cell, return a default cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReplacementProductTableViewCell", for: indexPath) as? ReplacementProductTableViewCell else {
            print("failed to get cell")
            return UITableViewCell()
        }
        
        if(indexPath.section == 0) {
            cell.imageviewProduct!.image = UIImage.convertBase64TypeToImage(imgString: arrayReplacementProduct[indexPath.row].image!)
            cell.labelProductName.text = arrayReplacementProduct[indexPath.row].productModelName
            let stringArray = arrayReplacementProduct[indexPath.row].applicationDetails!.components(separatedBy: "|").filter({ $0 != ""})
            
            cell.labelInstructions.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))
            cell.buttonLessAndMore.tag = indexPath.row
            cell.buttonLessAndMore.accessibilityIdentifier = String(indexPath.section)
            
            cell.buttonLessAndMore.addTarget(self, action: #selector(testFunc(button:)), for: .touchUpInside)
            
            // if the cell is expanded
            if expandedSectionOneIndexSet.contains(indexPath.row) {
                // the label can take as many lines it need to display all text
                cell.labelInstructions.numberOfLines = 0
                cell.labelInstructions.isHidden = false
                cell.buttonLessAndMore.setTitle("Less", for: .normal)
            } else {
                // if the cell is contracted
                // only show first 3 lines
                cell.labelInstructions.numberOfLines = 1
                cell.labelInstructions.isHidden = true
                cell.buttonLessAndMore.setTitle("More", for: .normal)
            }
            
        } else {
            cell.imageviewProduct!.image = UIImage.convertBase64TypeToImage(imgString: arrayCompatibleProduct[indexPath.row].image!)
            cell.labelProductName.text = arrayCompatibleProduct[indexPath.row].productModelName
            
            let stringArray = arrayCompatibleProduct[indexPath.row].applicationDetails!.components(separatedBy: "|").filter({ $0 != ""})
            
            cell.labelInstructions.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))

            cell.buttonLessAndMore.tag = indexPath.row
            cell.buttonLessAndMore.accessibilityIdentifier = String(indexPath.section)
            
            cell.buttonLessAndMore.addTarget(self, action: #selector(testFunc(button:)), for: .touchUpInside)
            
            // if the cell is expanded
            if expandedSectionTwoIndexSet.contains(indexPath.row) {
                // the label can take as many lines it need to display all text
                cell.labelInstructions.numberOfLines = 0
                cell.labelInstructions.isHidden = false
                cell.buttonLessAndMore.setTitle("Less", for: .normal)
                
                
            } else {
                // if the cell is contracted
                // only show first 3 lines
                cell.labelInstructions.numberOfLines = 1
                cell.labelInstructions.isHidden = true
                cell.buttonLessAndMore.setTitle("More", for: .normal)
                
            }
        }
        
        return cell
    }
}

extension ReplacementProductViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(indexPath.section == 0) {
            navigateToDoubleWiredDetailView(aSelectedRecord: arrayReplacementProduct[indexPath.row], isReplacementProduct: true)
        } else {
            navigateToDoubleWiredDetailView(aSelectedRecord: arrayCompatibleProduct[indexPath.row], isReplacementProduct: false)
        }
        
    }
    
    func navigateToDoubleWiredDetailView (aSelectedRecord : Product, isReplacementProduct : Bool) {
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let doubleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "DoubleWiredDetailViewController") as! DoubleWiredDetailViewController
//        doubleWiredDetailtViewController.aProduct = aSelectedRecord
//        doubleWiredDetailtViewController.rootProduct = selectedProduct
//        doubleWiredDetailtViewController.isreplacementProduct = isReplacementProduct
//        doubleWiredDetailtViewController.arrayProduct = arrayProduct
//        self.navigationController?.pushViewController(doubleWiredDetailtViewController, animated: true)
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let doubleWiredDetailtViewController = storyBoard.instantiateViewController(withIdentifier: "DoubleWiredDetailTableViewController") as! DoubleWiredDetailTableViewController
        doubleWiredDetailtViewController.aProduct = aSelectedRecord
        doubleWiredDetailtViewController.rootProduct = selectedProduct
        doubleWiredDetailtViewController.isreplacementProduct = isReplacementProduct
        doubleWiredDetailtViewController.arrayProduct = arrayProduct
        self.navigationController?.pushViewController(doubleWiredDetailtViewController, animated: true)
    }
    
}


extension String
{
    func trimed() -> String
    {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
}

