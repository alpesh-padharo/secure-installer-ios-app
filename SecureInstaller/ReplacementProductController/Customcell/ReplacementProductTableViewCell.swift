//
//  ReplacementProductTableViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit

class ReplacementProductTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonLessAndMore: UIButton!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var labelInstructions: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        labelProductName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelInstructions?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        buttonLessAndMore.titleLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 12)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
