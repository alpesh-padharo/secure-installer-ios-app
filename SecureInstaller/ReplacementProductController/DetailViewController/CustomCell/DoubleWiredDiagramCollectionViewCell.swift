//
//  DoubleWiredDiagramCollectionViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 22/10/20.
//

import UIKit

class DoubleWiredDiagramCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var labelSingleLetterTop: UILabel!
    @IBOutlet weak var labelMultiLetterTop: UILabel!
    @IBOutlet weak var labelSingleLetterBottom: UILabel!
    @IBOutlet weak var labelMultiLetterBottom: UILabel!
    @IBOutlet weak var viewConnector: UIView!
}
