//
//  DWWiringAndBackplateCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit

class DWWiringAndBackplateCell: UITableViewCell {
    @IBOutlet weak var labelStaticName: UILabel!
    @IBOutlet weak var labelValue: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelValue?.font = UIFont(name: "Kohinoor Devanagari", size: 12)

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
