//
//  DWProductNameViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit

class DWProductNameViewCell: UITableViewCell {

    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelStaticName: UILabel!
    @IBOutlet weak var LabelProductName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        labelStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        LabelProductName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
