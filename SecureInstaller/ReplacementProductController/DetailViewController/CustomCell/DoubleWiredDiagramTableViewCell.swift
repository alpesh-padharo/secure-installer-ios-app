//
//  DoubleWiredDiagramTableViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 22/10/20.
//

import UIKit

class DoubleWiredDiagramTableViewCell: UITableViewCell {

    @IBOutlet weak var labelRootProductname: UILabel!
    @IBOutlet weak var labelProductname: UILabel!
    @IBOutlet weak var labelStaticName: UILabel!
    @IBOutlet weak var viewBackground1, viewBackground2: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
