//
//  DWWiringDiagramViewCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit

class DWWiringDiagramViewCell: UITableViewCell {

    @IBOutlet weak var labelRootProductname: UILabel!
    @IBOutlet weak var labelProductname: UILabel!
    
    @IBOutlet weak var labelStaticName: UILabel!
    
    @IBOutlet weak var viewBackground1, viewBackground2: UIView!
    
    @IBOutlet weak var collectionViewDoubleWiredProduct: UICollectionView!
    @IBOutlet weak var collectionViewDoubleWiredRootProduct: UICollectionView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewBackground1.layer.borderWidth = 2
        viewBackground1.layer.borderColor = UIColor.gray.cgColor

        viewBackground2.layer.borderWidth = 2
        viewBackground2.layer.borderColor = UIColor.gray.cgColor

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
