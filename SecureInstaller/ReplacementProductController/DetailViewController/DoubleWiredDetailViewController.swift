//
//  DoubleWiredDetailViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 20/10/20.
//

import UIKit
import SafariServices

class DoubleWiredDetailViewController: UIViewController, UICollectionViewDelegate {

    var rootProduct : Product = Product()
    var aProduct : Product = Product()
    var aRootProduct : Product = Product()
    var isreplacementProduct : Bool = false
    internal var arrayProduct = [Product]()

    @IBOutlet weak var tableDoubleWired: UITableView!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navBarModifications()
        tableDoubleWired.delegate = self
        tableDoubleWired.dataSource = self
        tableDoubleWired.tableFooterView = UIView()
        
        var lastSearchedArray = DocumentFileManager.shared.getStoredlastSearched()
        if(!lastSearchedArray.contains(String(aProduct.id))){
            lastSearchedArray.append(String(aProduct.id))
            DocumentFileManager.shared.saveLastSearched(array: lastSearchedArray)
        }
        
        aRootProduct = findRootProduct(isReplacementProduct: isreplacementProduct)
    }
    
    func findRootProduct (isReplacementProduct : Bool) -> Product {
        
        let aSimilerProducts  = arrayProduct.filter { $0.manufacturerName!.contains(rootProduct.manufacturerName!) }
        
        let aSimilerProducts1  = aSimilerProducts.filter { $0.productModelName! == rootProduct.productModelName! }
        
        
        if(isReplacementProduct) {
            let aSimilerProducts2  = aSimilerProducts1.filter { $0.recommendProductName!.contains(aProduct.productModelName!) }
            let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
            if(arrayUniqueProduct1.count > 0){
                return arrayUniqueProduct1.first!
            } else {
                return rootProduct
            }
        } else {
            let aSimilerProducts2  = aSimilerProducts1.filter { $0.otherCompatibleProductName!.contains(aProduct.productModelName!) }
            let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
            if(arrayUniqueProduct1.count > 0){
                return arrayUniqueProduct1.first!
            } else {
                let token = aProduct.productModelName!.components(separatedBy: " ")
                if(token.count > 1) {
                    let aSimilerProducts2  = aSimilerProducts1.filter { $0.otherCompatibleProductName!.contains(token[1]) }
                    let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
                    
                    if(arrayUniqueProduct1.count > 0){
                        return arrayUniqueProduct1.first!
                    } else {
                        return rootProduct
                    }
                } else {
                    return rootProduct
                }
            }
            
        }
        
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)

    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
             [NSAttributedString.Key.foregroundColor: UIColor.white,
              NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 17)!]
        self.title = "Replace Programmer"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
               backBarButton.tintColor = .white
               self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
     @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func testFunc(button: UIButton){
        showTutorial()
    }
    
    func showTutorial() {
        if let url = URL(string:K.ProductionServer.staticUrl) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }



    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension DoubleWiredDetailViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return 94
        } else if (indexPath.row == 1) {
            return 60
        }else if (indexPath.row == 2) {
            
            if(aProduct.instructions!.count <= 0) {
                return 37
            } else {
                return 147
            }
            
        }else if (indexPath.row == 3) {
            return 292
        }else if (indexPath.row == 4) {
            return 81
        } else {
            return 44
        }
        
    }
}

extension DoubleWiredDetailViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            // ProductNameViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "DWProductNameViewCell") as! DWProductNameViewCell?
            
            cell?.LabelProductName?.text = aProduct.productModelName!
            cell?.imageViewProduct?.image = UIImage.convertBase64TypeToImage(imgString: aProduct.image!)

            return cell ?? UITableViewCell()
        } else if (indexPath.row == 1) {
            // WiringAndBackplateCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "DWWiringAndBackplateCell") as! DWWiringAndBackplateCell?
            cell?.labelValue?.text = aProduct.wiringAndBackPlate!
            return cell ?? UITableViewCell()
        }else if (indexPath.row == 2) {
            // InstructionsViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "DWInstructionsViewCell") as! DWInstructionsViewCell?
            let stringArray = aProduct.instructions!.components(separatedBy: "|").filter({ $0 != ""})
            
            cell?.textViewInstructions?.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))
            return cell ?? UITableViewCell()
        }else if (indexPath.row == 3) {
            // WiringDiagramViewCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "DWWiringDiagramViewCell") as! DWWiringDiagramViewCell?
            cell?.collectionViewDoubleWiredProduct?.delegate = self
            cell?.collectionViewDoubleWiredProduct?.dataSource = self
            
            
            cell?.collectionViewDoubleWiredRootProduct?.delegate = self
            cell?.collectionViewDoubleWiredRootProduct?.dataSource = self
                        
            cell?.labelRootProductname?.text = "\(aRootProduct.manufacturerName ?? "")\(rootProduct.productModelName ?? "")"
            cell?.labelProductname?.text = "\(aProduct.productModelName ?? "")"
            
            return cell ?? UITableViewCell()
        }else if (indexPath.row == 4) {
            
            return UITableViewCell()
        }else
        {
            return UITableViewCell()
        }
    }
    
    
}
extension DoubleWiredDetailViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if(collectionView.tag == 101) {
            return CGSize(width: 20, height: 70)
        } else {
            return CGSize(width: 20, height: 120)
        }

    }
}

extension DoubleWiredDetailViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if(collectionView.tag == 102) {
            return 13
        } else {
            return 12
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if(collectionView.tag == 102) {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DWCollectionViewCell1", for: indexPath) as! DWCollectionViewCell1
            cell.labelTop.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
            
            cell.labelTop.layer.backgroundColor  = UIColor.gray.cgColor
            cell.labelTop.layer.cornerRadius = 10
            cell.labelTop.layer.masksToBounds = true
            cell.labelTop.backgroundColor = UIColor.white

            if(indexPath.row == 0) {
                            
                if(aRootProduct.pair1!.count <= 0){
                    cell.isHidden = true
                }else {
                    cell.labelTop.text = aRootProduct.pair1
                }
                
                
            } else if (indexPath.row == 1) {
                if(aRootProduct.pair2!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair2
                }
                
            }else if (indexPath.row == 2) {
            
                if(aRootProduct.pair3!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair3
                }
                
            }else if (indexPath.row == 3) {
                if(aRootProduct.pair4!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair4
                }
            }else if (indexPath.row == 4) {
                if(aRootProduct.pair5!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair5
                }
            }else if (indexPath.row == 5) {
                if(aRootProduct.pair6!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair6
                }
            }else if (indexPath.row == 6) {
                if(aRootProduct.pair7!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair7
                }
            }else if (indexPath.row == 7) {
                
                if(aRootProduct.pair8!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair8
                }
            }else if (indexPath.row == 8) {
                if(aRootProduct.pair9!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair9
                }
            }else if (indexPath.row == 9) {
                if(aRootProduct.pair10!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair10
                }
            }else if (indexPath.row == 10) {
                if(aRootProduct.pair11!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair11
                }
            }else if (indexPath.row == 11) {
                if(aRootProduct.pair12!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.pair12
                }
            } else if (indexPath.row == 12) {
                if(aRootProduct.spare_pin?.count ?? 0 <= 0) {
                    cell.isHidden = true
                } else {
                    cell.labelTop.text = aRootProduct.spare_pin
                    
                    cell.viewBottom.isHidden = true
                }
            }

            return cell
        } else {
         
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DWCollectionViewCell2", for: indexPath) as! DWCollectionViewCell2
            cell.labelTop.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
            cell.labelBottom.font = UIFont(name: "Kohinoor Devanagari", size: 6)!
            cell.labelBottom.adjustsFontSizeToFitWidth = true
            
            cell.labelTop.layer.backgroundColor  = UIColor.gray.cgColor
            cell.labelTop.layer.cornerRadius = 10
            cell.labelTop.layer.masksToBounds = true
            cell.labelTop.backgroundColor = UIColor.white

            if(indexPath.row == 0) {
                
                cell.labelTop.text = "E"
                
                if(aProduct.pair1!.count <= 0){
                    cell.labelBottom.text = "E"
                }else {
                    cell.labelBottom.text = aProduct.pair1
                }
                
                
            } else if (indexPath.row == 1) {
                cell.labelTop.text = "N"
                if(aProduct.pair2!.count <= 0){
                    cell.labelBottom.text = "N"
                } else {
                    cell.labelBottom.text = aProduct.pair2
                }
                
            }else if (indexPath.row == 2) {
                cell.labelTop.text = "L"
                if(aProduct.pair3!.count <= 0){
                    cell.labelBottom.text = "L"
                } else {
                    cell.labelBottom.text = aProduct.pair3
                }
                
            }else if (indexPath.row == 3) {
                cell.labelTop.text = "4"
                if(aProduct.pair4!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair4
                }
            }else if (indexPath.row == 4) {
                cell.labelTop.text = "5"
                if(aProduct.pair5!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair5
                }
            }else if (indexPath.row == 5) {
                cell.labelTop.text = "6"
                if(aProduct.pair6!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair6
                }
            }else if (indexPath.row == 6) {
                cell.labelTop.text = "7"
                if(aProduct.pair7!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair7
                }
            }else if (indexPath.row == 7) {
                cell.labelTop.text = "8"
                if(aProduct.pair8!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair8
                }
            }else if (indexPath.row == 8) {
                cell.labelTop.text = "9"
                if(aProduct.pair9!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair9
                }
            }else if (indexPath.row == 9) {
                cell.labelTop.text = "10"
                if(aProduct.pair10!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair10
                }
            }else if (indexPath.row == 10) {
                cell.labelTop.text = "11"
                if(aProduct.pair11!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair11
                }
            }else if (indexPath.row == 11) {
                cell.labelTop.text = "12"
                if(aProduct.pair12!.count <= 0){
                    cell.isHidden = true
                } else {
                    cell.labelBottom.text = aProduct.pair12
                }
            }

            return cell
        }
    }

}
extension DoubleWiredDetailViewController {
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            guard let windowInterfaceOrientation = self.windowInterfaceOrientation else { return }
            
            if windowInterfaceOrientation.isLandscape {
                // activate landscape changes
                
            } else {
                // activate portrait changes
            }
            self.tableDoubleWired.reloadData()
        })
    }
    
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            // Fallback on earlier versions
            return UIApplication.shared.statusBarOrientation
        }
    }
}


class NSAttributedStringHelper {
    static func createBulletedList(fromStringArray strings: [String], font: UIFont? = nil) -> NSAttributedString {

        let fullAttributedString = NSMutableAttributedString()
        let attributesDictionary: [NSAttributedString.Key: Any]

        if font != nil {
            attributesDictionary = [NSAttributedString.Key.font: font!]
        } else {
            attributesDictionary = [NSAttributedString.Key: Any]()
        }

        for index in 0..<strings.count {
            let bulletPoint: String = "\u{2022}"
            var formattedString: String = "\(bulletPoint) \(strings[index])"

            if index < strings.count - 1 {
                formattedString = "\(formattedString)\n"
            }

            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: formattedString, attributes: attributesDictionary)
            let paragraphStyle = NSAttributedStringHelper.createParagraphAttribute()
   attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: paragraphStyle], range: NSMakeRange(0, attributedString.length))
        fullAttributedString.append(attributedString)
       }

        return fullAttributedString
    }

    private static func createParagraphAttribute() -> NSParagraphStyle {
        let paragraphStyle: NSMutableParagraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.tabStops = [NSTextTab(textAlignment: .left, location: 15, options: NSDictionary() as! [NSTextTab.OptionKey : Any])]
        paragraphStyle.defaultTabInterval = 15
        paragraphStyle.firstLineHeadIndent = 0
        paragraphStyle.headIndent = 11
        return paragraphStyle
    }
}
