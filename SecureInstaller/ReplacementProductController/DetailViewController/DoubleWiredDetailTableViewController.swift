//
//  DoubleWiredDetailTableViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 22/10/20.
//

import UIKit
import DeviceGuru

class DoubleWiredDetailTableViewController: UITableViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {
    
    var cellsPerRow:CGFloat = 14
    var cellPadding:CGFloat = 2

    var rootProduct : Product = Product()
    var aProduct : Product = Product()
    var aRootProduct : Product = Product()
    var isreplacementProduct : Bool = false
    internal var arrayProduct = [Product]()
    
    @IBOutlet weak var collectionViewControllerWired: UICollectionView!
    
    // Cell 1
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelProductStaticName: UILabel!
    @IBOutlet weak var topLabelProductName: UILabel!
    
    // cell 2
    @IBOutlet weak var labelWiringStaticName: UILabel!
    @IBOutlet weak var labelWiringValue: UILabel!
    
    // cell 3
    @IBOutlet weak var textViewInstructions: UITextView!
    @IBOutlet weak var labelInstructionsStaticName: UILabel!
    
    @IBOutlet weak var labelRootProductname: UILabel!
    @IBOutlet weak var labelProductname: UILabel!
    @IBOutlet weak var labelWiringDiagramStaticName: UILabel!
    @IBOutlet weak var viewBackground1, viewBackground2: UIView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        navBarModifications()
        
        var lastSearchedArray = DocumentFileManager.shared.getStoredlastSearched()
        if(!lastSearchedArray.contains(String(aProduct.id))){
            lastSearchedArray.append(String(aProduct.id))
            DocumentFileManager.shared.saveLastSearched(array: lastSearchedArray)
        }
        
        aRootProduct = findRootProduct(isReplacementProduct: isreplacementProduct)
        
        if(aRootProduct.spare_pin != nil) {
            
            if(aRootProduct.spare_pin?.count ?? 0 <= 0) {
                cellsPerRow = 10
            } else {
                cellsPerRow = 14
                
            }
            
        } else {
            cellsPerRow = 10
        }
        
        collectionViewControllerWired.dataSource = self
        collectionViewControllerWired.delegate = self
        collectionViewControllerWired.reloadData()
        
        // Update cell info
        
        updateProductNameCell()
        updateWiringAndBackplateCell()
        updateInstructionsCell()
        updateDiagramCell()
        
    }
    
    func updateProductNameCell ()
    {
        labelProductStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        topLabelProductName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        
        topLabelProductName?.text = aProduct.productModelName
        imageViewProduct?.image = UIImage.convertBase64TypeToImage(imgString: aProduct.image!)
    }
    
    func updateWiringAndBackplateCell ()
    {
        labelWiringStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelWiringValue?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        
        labelWiringValue.text = aRootProduct.wiringAndBackPlate
        
    }
    
    func updateInstructionsCell ()
    {
        labelInstructionsStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        textViewInstructions?.font = UIFont(name: "Kohinoor Devanagari", size: 11)
        
        let stringArray = aProduct.instructions!.components(separatedBy: "|").filter({ $0 != ""})
        
        textViewInstructions.attributedText = NSAttributedStringHelper.createBulletedList(fromStringArray: stringArray, font: UIFont(name: "Kohinoor Devanagari", size: 11))
        
    }
    
    func updateDiagramCell ()
    {
        labelWiringDiagramStaticName?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelRootProductname?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        labelProductname?.font = UIFont(name: "Kohinoor Devanagari", size: 12)


        labelWiringDiagramStaticName.text = "Wiring Diagram"
        labelRootProductname.text = "\(aRootProduct.manufacturerName ?? "") \(rootProduct.productModelName ?? "")"
        labelProductname.text = "\(aProduct.productModelName ?? "")"
        
        labelRootProductname.textColor = UIColor.gray
        
        viewBackground1.layer.borderWidth = 1
        viewBackground1.layer.borderColor = UIColor.gray.cgColor
        
        viewBackground2.layer.borderWidth = 1
        viewBackground2.layer.borderColor = UIColor.gray.cgColor
    }
    
    func findRootProduct (isReplacementProduct : Bool) -> Product {
        
        let aSimilerProducts  = arrayProduct.filter { $0.manufacturerName!.contains(rootProduct.manufacturerName!) }
        let aSimilerProducts1  = aSimilerProducts.filter { $0.productModelName! == rootProduct.productModelName! }
        
        if(isReplacementProduct) {
            let aSimilerProducts2  = aSimilerProducts1.filter { $0.recommendProductName!.contains(aProduct.productModelName!) }
            let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
            if(arrayUniqueProduct1.count > 0){
                return arrayUniqueProduct1.first!
            } else {
                return rootProduct
            }
        } else {
            let aSimilerProducts2  = aSimilerProducts1.filter { $0.otherCompatibleProductName!.contains(aProduct.productModelName!) }
            let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
            if(arrayUniqueProduct1.count > 0){
                return arrayUniqueProduct1.first!
            } else {
                let token = aProduct.productModelName!.components(separatedBy: " ")
                if(token.count > 1) {
                    let aSimilerProducts2  = aSimilerProducts1.filter { $0.otherCompatibleProductName!.contains(token[1]) }
                    let arrayUniqueProduct1  = aSimilerProducts2.unique{$0.productModelName}
                    
                    if(arrayUniqueProduct1.count > 0){
                        return arrayUniqueProduct1.first!
                    } else {
                        return rootProduct
                    }
                } else {
                    return rootProduct
                }
            }
        }
    }
    
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 17)!]
        self.title = "Replace Programmer"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem?
        backBarButton?.tintColor = .white
        self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem?
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 4
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 0) {
            return 94
        } else if (indexPath.row == 1) {
            if(aRootProduct.wiringAndBackPlate!.count <= 0) {
                    return 20
                } else {
                    return 60
                }
            
        }else if (indexPath.row == 2) {
            
            if(aProduct.instructions!.count <= 0) {
                return 37
            } else {
                return 137
            }
            
        }else if (indexPath.row == 3) {
            return 280
        }else if (indexPath.row == 4) {
            return 81
        } else {
            return 44
        }
        
    }
}

extension DoubleWiredDetailTableViewController {
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
        coordinator.animate(alongsideTransition: { (context) in
            guard let windowInterfaceOrientation = self.windowInterfaceOrientation else { return }
            
            if windowInterfaceOrientation.isLandscape {
                // activate landscape changes
                
            } else {
                // activate portrait changes
            }
            self.collectionViewControllerWired.collectionViewLayout.invalidateLayout()
        })
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var SpaceNeedToRemove = 32
        if (UIDevice.current.hasNotch) {
            //... consider notch
            if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight) {
                SpaceNeedToRemove = SpaceNeedToRemove + 80
            } else {
                cellPadding = 0
            }
        }
        
        let deviceGuru = DeviceGuru()
        let deviceName = deviceGuru.hardware()

        if(deviceName == .iphone_xr) {
            SpaceNeedToRemove = SpaceNeedToRemove - 20
            cellPadding = 4
        }
        
        let widthMinusPadding = UIScreen.main.bounds.width - CGFloat(SpaceNeedToRemove) - (cellPadding + cellPadding * cellsPerRow)
        
        let eachSide = widthMinusPadding / cellsPerRow
        return CGSize(width: eachSide, height: 190)
        
        
    }
    func getcellWidth() -> CGFloat {
        var SpaceNeedToRemove = 32
        if (UIDevice.current.hasNotch) {
            //... consider notch
            if(UIDevice.current.orientation == .landscapeLeft || UIDevice.current.orientation == .landscapeRight) {
                SpaceNeedToRemove = SpaceNeedToRemove + 80
            } else {
                cellPadding = 0
            }
        }
        
        let deviceGuru = DeviceGuru()
        let deviceName = deviceGuru.hardware()

        if(deviceName == .iphone_xr) {
            SpaceNeedToRemove = SpaceNeedToRemove - 20
            cellPadding = 4
        }
        
        let widthMinusPadding = UIScreen.main.bounds.width - CGFloat(SpaceNeedToRemove) - (cellPadding + cellPadding * cellsPerRow)
        
        let eachSide = widthMinusPadding / cellsPerRow
        
        return eachSide
    }
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            // Fallback on earlier versions
            return UIApplication.shared.statusBarOrientation
        }
    }
}

extension DoubleWiredDetailTableViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoubleWiredDiagramCollectionViewCell", for: indexPath) as! DoubleWiredDiagramCollectionViewCell
        
        cell.labelSingleLetterTop.font = UIFont(name: "Kohinoor Devanagari", size: 8)!
        cell.labelSingleLetterBottom.font = UIFont(name: "Kohinoor Devanagari", size: 10)!
        cell.labelMultiLetterBottom.font = UIFont(name: "Kohinoor Devanagari", size: 6)!
        
        cell.labelMultiLetterTop.font = UIFont(name: "Kohinoor Devanagari", size: 6)!
        cell.labelMultiLetterTop.isHidden = true

        //cell.labelMultiLetterBottom.a
        
        cell.labelSingleLetterTop.layer.cornerRadius = 10
        cell.labelSingleLetterTop.layer.masksToBounds = true
        cell.labelSingleLetterTop.backgroundColor = UIColor.white
        
        cell.labelSingleLetterBottom.layer.cornerRadius = 10
        cell.labelSingleLetterBottom.layer.masksToBounds = true
        cell.labelSingleLetterBottom.backgroundColor = UIColor.white
        
        cell.labelSingleLetterBottom.layer.borderWidth = 1
        cell.labelSingleLetterBottom.layer.borderColor = UIColor.gray.cgColor
        
        cell.labelSingleLetterTop.layer.borderWidth = 1
        cell.labelSingleLetterTop.layer.borderColor = UIColor.gray.cgColor

        
        if(indexPath.row == 0) {
            
            if(aRootProduct.pair1!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            }else {
                cell.labelSingleLetterTop.text = aRootProduct.pair1
            }
            
            cell.labelSingleLetterBottom.text = "E"
            
            if(aProduct.pair1!.count <= 0){
                cell.labelMultiLetterBottom.text = "E"
            }else {
                cell.labelMultiLetterBottom.text = aProduct.pair1
                cell.labelMultiLetterBottom.text = "E"
            }
            
        } else if (indexPath.row == 1) {
            if(aRootProduct.pair2!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair2
            }
            
            cell.labelSingleLetterBottom.text = "N"
            if(aProduct.pair2!.count <= 0){
                cell.labelMultiLetterBottom.text = "N"
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair2
                cell.labelMultiLetterBottom.text = "N"
            }
            
        }else if (indexPath.row == 2) {
            
            if(aRootProduct.pair3!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair3
            }
            
            cell.labelSingleLetterBottom.text = "L"
            if(aProduct.pair3!.count <= 0){
                cell.labelMultiLetterBottom.text = "L"
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair3
                cell.labelMultiLetterBottom.text = "L"
            }
            
        }else if (indexPath.row == 3) {
            if(aRootProduct.pair4!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair4
            }
            
            cell.labelSingleLetterBottom.text = "4"
            if(aProduct.pair4!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair4
            }
        }else if (indexPath.row == 4) {
            if(aRootProduct.pair5!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair5
            }
            
            cell.labelSingleLetterBottom.text = "5"
            if(aProduct.pair5!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair5
            }
        }else if (indexPath.row == 5) {
            if(aRootProduct.pair6!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair6
            }
            
            cell.labelSingleLetterBottom.text = "6"
            if(aProduct.pair6!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair6
            }
        }else if (indexPath.row == 6) {
            if(aRootProduct.pair7!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair7
            }
            
            cell.labelSingleLetterBottom.text = "7"
            if(aProduct.pair7!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair7
            }
        }else if (indexPath.row == 7) {
            
            if(aRootProduct.pair8!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair8
            }
            
            cell.labelSingleLetterBottom.text = "8"
            if(aProduct.pair8!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair8
            }
        }else if (indexPath.row == 8) {
            if(aRootProduct.pair9!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair9
            }
            
            cell.labelSingleLetterBottom.text = "9"
            if(aProduct.pair9!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair9
            }
        }else if (indexPath.row == 9) {
            if(aRootProduct.pair10!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair10
            }
            
            cell.labelSingleLetterBottom.text = "10"
            if(aProduct.pair10!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair10
            }
        }else if (indexPath.row == 10) {
            if(aRootProduct.pair11!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair11
            }
            
            cell.labelSingleLetterBottom.text = "11"
            if(aProduct.pair11!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair11
            }
        }else if (indexPath.row == 11) {
            if(aRootProduct.pair12!.count <= 0){
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.pair12
            }
            
            cell.labelSingleLetterBottom.text = "12"
            if(aProduct.pair12!.count <= 0){
                cell.labelMultiLetterBottom.isHidden = true
                cell.labelSingleLetterBottom.isHidden = true
            } else {
                cell.labelMultiLetterBottom.text = aProduct.pair12
            }
        } else if (indexPath.row == 12) {
            if(aRootProduct.spare_pin?.count ?? 0 <= 0) {
                cell.labelSingleLetterTop.isHidden = true
                cell.viewConnector.isHidden = true
            } else {
                cell.labelSingleLetterTop.text = aRootProduct.spare_pin
                cell.labelMultiLetterTop.text = "SPARE"
                cell.labelMultiLetterTop.isHidden = false
                cell.viewConnector.isHidden = true
            }
            cell.labelMultiLetterBottom.isHidden = true
            cell.labelSingleLetterBottom.isHidden = true
        }
        
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 13
    }
}



extension UIDevice {
    var hasNotch: Bool {
        let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
        return bottom > 0
    }
}
