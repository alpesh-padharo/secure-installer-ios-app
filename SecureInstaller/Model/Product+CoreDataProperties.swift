//
//  Product+CoreDataProperties.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 19/10/20.
//
//

import Foundation
import CoreData


extension Product {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }

    @NSManaged public var applicationDetails: String?
    @NSManaged public var createDate: Date?
    @NSManaged public var faqurl: String?
    @NSManaged public var id: Int64
    @NSManaged public var image: String?
    @NSManaged public var instructions: String?
    @NSManaged public var isActive: Bool
    @NSManaged public var isCommissioningProduct: String?
    @NSManaged public var isNewInstall: String?
    @NSManaged public var manufacturerName: String?
    @NSManaged public var modelImagePath: String?
    @NSManaged public var otherCompatibleProductName: String?
    @NSManaged public var otherUrl: String?
    @NSManaged public var pair1: String?
    @NSManaged public var pair2: String?
    @NSManaged public var pair3: String?
    @NSManaged public var pair4: String?
    @NSManaged public var pair5: String?
    @NSManaged public var pair6: String?
    @NSManaged public var pair7: String?
    @NSManaged public var pair8: String?
    @NSManaged public var pair9: String?
    @NSManaged public var pair10: String?
    @NSManaged public var pair11: String?
    @NSManaged public var pair12: String?
    @NSManaged public var productCategory: String?
    @NSManaged public var productModelName: String?
    @NSManaged public var productSubCategory: String?
    @NSManaged public var recommendProductInstructions: String?
    @NSManaged public var recommendProductName: String?
    @NSManaged public var spare_pin: String?
    @NSManaged public var updateDate: Date?
    @NSManaged public var version: Float
    @NSManaged public var videoURL: String?
    @NSManaged public var wiringAndBackPlate: String?

}

