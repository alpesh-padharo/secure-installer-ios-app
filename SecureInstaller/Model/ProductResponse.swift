//
//  ProductResponse.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 13/10/20.
//

import Foundation

struct ProductApiResponse : Decodable {
    let responseCode : Int64
    let dbTableVersion : Int64
    let recordCount : Int64
    let responseObj : [ResponseObj]
    
    enum CodingKeys: String, CodingKey {
        case responseCode
        case dbTableVersion
        case recordCount
        case responseObj
    }
}

// MARK: - ResponseObj
struct ResponseObj: Decodable {
    let manufacturerName: String
    let productModelName, pair1, pair2, pair3: String
    let pair4, pair5, pair6, pair7: String
    let pair8, pair9, pair10, pair11: String
    let pair12, sparePin: String
    let otherCompatibleProductName: String
    let recommendProductName: String
    let productCategory: String
    let productSubCategory: String
    let modelImagePath: String
    let image, applicationDetails: String
    let wiringAndBackPlate: String
    let instructions: String
    let isNewInstall, isCommissioningProduct: String
    let faqurl, videoURL, otherURL: String
    let isActive: Bool
    let version: Int
    let privacyPolicyURL, contactDetails: String
    let id: Int
    let createDate, updateDate: String

    enum CodingKeys: String, CodingKey {
        case manufacturerName, productModelName, pair1, pair2, pair3, pair4, pair5, pair6, pair7, pair8, pair9, pair10, pair11, pair12
        case sparePin = "spare_Pin"
        case otherCompatibleProductName, recommendProductName, productCategory, productSubCategory, modelImagePath, image, applicationDetails, wiringAndBackPlate, instructions, isNewInstall, isCommissioningProduct, faqurl, videoURL
        case otherURL = "otherUrl"
        case isActive, version, privacyPolicyURL, contactDetails, id, createDate, updateDate
    }
}
