//
//  Constants.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 13/10/20.
//

import Foundation
import UIKit

struct K {
    struct ProductionServer {
        static let baseURL = "http://staging.fivesdigital.com:8080/installerapp/api"
        
       // static let baseURL = "http://172.16.16.43/installerapp/api"
  
        static let staticUrl = "http://horstmann.securemeters.com/index.php/index.php/product/heating-and-hot-water-controls/timeswitches-programmers/channelplus-xl/"

    }
    
    struct APIParameterKey {
        static let password = "password"
        static let email = "email"
        static let token = "Authorization"
    }
}



enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/json"
}

