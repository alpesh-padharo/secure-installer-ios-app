//
//  DashboardCell.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 14/10/20.
//

import UIKit

class DashboardCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        title?.layer.cornerRadius = 5
        title?.layer.masksToBounds = true
        title?.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        
        let view = UIView()
        view.backgroundColor = UIColor.clear
        selectedBackgroundView = view

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
