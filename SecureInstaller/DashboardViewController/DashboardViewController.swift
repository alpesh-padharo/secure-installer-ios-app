//
//  DashboardViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 14/10/20.
//

import UIKit

import KRProgressHUD
import SystemConfiguration

class DashboardViewController: UIViewController {
    
    // MARK: - Properties

    
    fileprivate enum Constants {
        static let DashhboardCellIdentifier = "DashboardCell"
    }
    
    fileprivate let dashboardHeaders = ["Installation",
                                        " "]
    
    fileprivate let dashboardSectionOneItems = ["  Replace a programmer or time switch",
                                                "  Install a new product",
                                                "",
                                                "  Product guide"]
    

    
    @IBOutlet weak var tableDashBoard: UITableView!
    
    fileprivate var products = [Product]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Do any additional setup after loading the view.
        
        if(!UIDevice.current.isJailBroken) {
            tableDashBoard.dataSource = self
            tableDashBoard.delegate = self
            tableDashBoard.tableFooterView = UIView()
            tableDashBoard.reloadData()
        }
        

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Setup Core Data Manager
        self.setupApplicationData()
                
    }
    
    func setupApplicationData(){
        
        if(!UIDevice.current.isJailBroken){
            self.navigationItem.leftBarButtonItem = nil
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                // your code here
                KRProgressHUD.show()
                let defaults = UserDefaults.standard
                let isDataCopied = defaults.bool(forKey: "isDataCopiedInLocalAppDirectory")
                if(!isDataCopied) {
                    ProductAPI.shared.copyBundleDataToLocalAppDirectory()
                }
                KRProgressHUD.dismiss()
            }
            
            
            navigationBarModifications()
            
            if(isConnectedToNetwork())
            {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    // your code here
                    ProductAPI.shared.getAuthToken { (authresponse) in
                        print("token ==>  \(authresponse.token)")
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                            ProductAPI.shared.getProducts(withToken: authresponse.token) { (aResponse) in
                                // print("Product Response ==>  \(aResponse)")
                            }
                        }
                    }
                }
            } else {
                
                // create the alert
                let alert = UIAlertController(title: "Alert", message: "Please check your internet connection.", preferredStyle: UIAlertController.Style.alert)

                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            // create the alert
            let alert = UIAlertController(title: "Alert", message: "This Device is JailBroken.", preferredStyle: UIAlertController.Style.alert)

            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

            // show the alert
            self.present(alert, animated: true, completion: nil)

        }

    }
    
    fileprivate func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        if flags.isEmpty {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }
    func navigationBarModifications () {
        self.navigationController?.navigationBar.barTintColor = hexStringToUIColor(hex: "#79265C")
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
        self.title = "Secure Controls"
        
    }
    
    // to convert hex string to UIColor
    private func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension DashboardViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.row == 2) {
            return 20.0
        } else {
            return 56.0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if #available(iOS 13.0, *) {
            (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor.systemBackground.withAlphaComponent(1.0)
        } else {
            // Fallback on earlier versions
        }
        (view as! UITableViewHeaderFooterView).textLabel?.textColor = UIColor.gray
        (view as! UITableViewHeaderFooterView).textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 15)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        if(indexPath.section == 0 && indexPath.row == 0){
            performSegue(withIdentifier: "ReplaceProgrammerSegue", sender: nil)
        } else if(indexPath.section == 0 && indexPath.row == 1){
            performSegue(withIdentifier: "InstallNewSegue", sender: nil)
        } else if(indexPath.section == 0 && indexPath.row == 3){
            performSegue(withIdentifier: "ProductGuideSegue", sender: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.products = ProductAPI.shared.getAllProducts()
        
        if (segue.identifier == "ReplaceProgrammerSegue") {
            let repalceController = segue.destination as? ReplaceProgrammerViewController
            repalceController?.arrayProduct = products
        } else if(segue.identifier == "ProductGuideSegue") {
            let productGuideController = segue.destination as? ProductGuideViewController
            productGuideController?.arrayProduct = products
        } else if(segue.identifier == "InstallNewSegue") {
            let installNewProductController = segue.destination as? InstallNewProductViewController
            installNewProductController?.arrayProduct = products
        }
    }
}

extension DashboardViewController : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return dashboardHeaders[0]
        } else {
            return dashboardHeaders[1]
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 4
        }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if(indexPath.section == 0) {
          
            if(indexPath.row == 2) {
                let cell = UITableViewCell()
                cell.selectionStyle = .none
                return cell
            } else{
                let cell = tableView.dequeueReusableCell(withIdentifier: Constants.DashhboardCellIdentifier, for: indexPath) as! DashboardCell
                cell.title?.text = dashboardSectionOneItems[indexPath.row]
                return cell
            }
            
            
        } else {
            
            let cell = UITableViewCell()
            cell.selectionStyle = .none
            return cell
            
        }
    }
    

}
 
