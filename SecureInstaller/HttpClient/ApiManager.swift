//
//  ApiManager.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 13/10/20.
//

import Foundation
import Alamofire
import SwiftyJSON
import KRProgressHUD

class APIClient {
    
    static let sharedManager = APIClient()
    
    private init() {}
    
    public func getAuthToken(withComplition authCompletionHandler : @escaping (AuthResponse)->() ){
        
        AF.sessionConfiguration.timeoutIntervalForRequest = 30

       // KRProgressHUD.show(withMessage: "Refreshing Token...")
        AF.request(APIRouter.token)
            .validate()
            .responseDecodable(of: AuthResponse.self) { response in
              // 4
                KRProgressHUD.dismiss()
              guard let authresponse = response.value else {
                ProductAPI.shared.saveProductDataInLocalStorage()
                return
                
              }
              authCompletionHandler(authresponse)
          }
    }
    
    public func getProductsData (withToken strToken:String, withComplition productCompletionHandler : @escaping (ProductApiResponse)->() ) {
        
       
        KRProgressHUD.show(withMessage: "Checking Version...")

        // use token
        AF.request(APIRouter.products(token: strToken))
            .validate()
            .responseDecodable(of: ProductApiResponse.self) { response in
                // 4
                guard let productApiResponse = response.value else {
                    ProductAPI.shared.saveProductDataInLocalStorage()
                    KRProgressHUD.dismiss()
                    return
                }
                
                
                if(ProductAPI.shared.getLocalDatbaseVersionNumber() != productApiResponse.dbTableVersion) {
                    // Update json to file
                    ProductAPI.shared.saveDataToLocalStorage(json: response.data!)
                    productCompletionHandler(productApiResponse)
                } else {
                    productCompletionHandler(productApiResponse)
                }
                ProductAPI.shared.saveProductDataInLocalStorage()
                KRProgressHUD.dismiss()

            }
    }
}



