//
//  ReplaceProgrammerViewController.swift
//  SecureInstaller
//
//  Created by Krupanshu Sharma on 15/10/20.
//

import UIKit
import KRProgressHUD
import EmptyDataSet_Swift

class ReplaceProgrammerViewController: UIViewController {
    
    @IBOutlet weak var tableHeaderView: UIView!
    @IBOutlet weak var tableHeaderLabel: UILabel!
    @IBOutlet weak var tableHeaderSearchTextfield: UITextField!{
        didSet {
            tableHeaderSearchTextfield.tintColor = UIColor.lightGray
            tableHeaderSearchTextfield.setIcon(#imageLiteral(resourceName: "zoom-out"))
        }
    }
    
    @IBOutlet weak var tableProduct: UITableView!
    internal var arrayProduct = [Product]()
    fileprivate var arrayUniqueProduct = [Product]()
    fileprivate var dictProductIndex = [String: [Product]]()
    fileprivate var arrayfilteredProductList: [Product] = []
    fileprivate var productSectionTitles = [String]()
    
    fileprivate var isSearching : Bool = false
    
    fileprivate enum Constants {
        static let MenuFecCellIdentifier = "MenuFecCell"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableProduct.delegate = self
        tableProduct.dataSource = self
        
        tableProduct.emptyDataSetSource = self
        tableProduct.emptyDataSetDelegate = self

        
        tableProduct.tableFooterView = UIView()
        // Do any additional setup after loading the view.
        prepareProducts()
        navBarModifications()
        setupUI()
    }
    
    
    fileprivate func setupUI() {
        tableHeaderLabel.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        tableHeaderSearchTextfield.font = UIFont(name: "Kohinoor Devanagari", size: 15)
        tableHeaderSearchTextfield.layer.borderWidth = 1
        tableHeaderSearchTextfield.layer.borderColor = UIColor(red: 241.0 / 255.0, green: 241.0 / 255.0, blue: 241.0 / 255.0, alpha: 1).cgColor
        tableHeaderSearchTextfield.backgroundColor = .clear
        tableHeaderSearchTextfield.leftViewMode = .always
        tableHeaderSearchTextfield.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                             for: .editingChanged)
        tableHeaderSearchTextfield.delegate = self
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        print(textField.text!)
        
        if(textField.text?.count ?? 0 > 0) {
            isSearching = true
        } else {
            isSearching = false
        }
        
        arrayfilteredProductList = arrayUniqueProduct.filter { $0.manufacturerName!.localizedCaseInsensitiveContains(textField.text!) }
        if (isSearching){
            dictProductIndex.removeAll()
            setContentForIndexedTableViewUsingModel(arrProducts: arrayfilteredProductList)
        } else {
            dictProductIndex.removeAll()
            setContentForIndexedTableViewUsingModel(arrProducts: arrayUniqueProduct)
        }
        tableProduct.reloadData()
    }
    
    fileprivate func prepareProducts() {
        
        arrayUniqueProduct = arrayProduct.unique{$0.manufacturerName}
        setContentForIndexedTableViewUsingModel(arrProducts: arrayUniqueProduct)
        tableProduct.reloadData()
    }
    
    fileprivate func setContentForIndexedTableViewUsingModel(arrProducts : [Product]) {
        
        for product in arrProducts {
            let productKey = String((product.manufacturerName?.prefix(1))!)
            if var productValues = dictProductIndex[productKey] {
                productValues.append(product)
                dictProductIndex[productKey] = productValues
            } else {
                dictProductIndex[productKey] = [product]
            }
        }
        
        productSectionTitles = [String](dictProductIndex.keys)
        productSectionTitles = productSectionTitles.sorted(by: { $0 < $1 })
    }
    
    fileprivate func navBarModifications() {
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.white,
             NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 18)!]
        self.title = "Replace Programmer"
        let backBarButton = UIBarButtonItem.init(image: UIImage(named: "back_arrow"), style: .plain, target: self, action: #selector(backButtonTapped(sender:))) as UIBarButtonItem
        backBarButton.tintColor = .white
        self.navigationItem.setLeftBarButton(backBarButton, animated: true)
        
        // setting right bar button
        let rightBarButton = UIBarButtonItem.init(image: UIImage(named: "home")?.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(homeTapped)) as UIBarButtonItem
        self.navigationItem.setRightBarButton(rightBarButton, animated: true)
        
    }
    
    @objc func homeTapped() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @objc func backButtonTapped(sender: UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ReplaceProgrammerViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension ReplaceProgrammerViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // 1
        return productSectionTitles.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let productKey = productSectionTitles[section]
        if let productValues = dictProductIndex[productKey] {
            return productValues.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 3
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MenuFecCellIdentifier, for: indexPath)
        cell.textLabel?.textColor = UIColor(red: 112 / 255.0, green: 112 / 255.0, blue: 112 / 255.0, alpha: 1)
        cell.textLabel?.font = UIFont(name: "Kohinoor Devanagari", size: 12)
        
        let productKey = productSectionTitles[indexPath.section]
        if var productValues = dictProductIndex[productKey] {
            productValues = productValues.sorted(by: { $0.manufacturerName! < $1.manufacturerName! })
            cell.textLabel?.text = productValues[indexPath.row].manufacturerName
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return productSectionTitles[section]
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 8, width: 320, height: 20)
        myLabel.font = UIFont(name: "Kohinoor Devanagari", size: 14)
        myLabel.textColor = UIColor(red: 121 / 255.0, green: 38 / 255.0, blue: 92 / 255.0, alpha: 1)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        
        let headerView = UIView()
        headerView.addSubview(myLabel)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productKey = productSectionTitles[indexPath.section]
        if var productValues = dictProductIndex[productKey] {
            productValues = productValues.sorted(by: { $0.manufacturerName! < $1.manufacturerName! })
            navigateToProductList(selectedRecord: productValues[indexPath.row])
        }
    }
    
    func navigateToProductList (selectedRecord : Product) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let productListViewController = storyBoard.instantiateViewController(withIdentifier: "ProductListViewController") as! ProductListViewController
        productListViewController.selectedProduct = selectedRecord
        productListViewController.arrayProduct = arrayProduct
        self.navigationController?.pushViewController(productListViewController, animated: true)
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return productSectionTitles
    }
}

extension UITextField {
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:
                                    CGRect(x: 10, y: 3, width: 14, height: 14))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:
                                                CGRect(x: 10, y: 0, width: 20, height: 20))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
    }
}

extension Array {
    func unique<T:Hashable>(by: ((Element) -> (T)))  -> [Element] {
        var set = Set<T>() //the unique list kept in a Set for fast retrieval
        var arrayOrdered = [Element]() //keeping the unique list of elements but ordered
        for value in self {
            if !set.contains(by(value)) {
                set.insert(by(value))
                arrayOrdered.append(value)
            }
        }

        return arrayOrdered
    }
}


extension ReplaceProgrammerViewController : EmptyDataSetSource, EmptyDataSetDelegate {
    
    //MARK: - DZNEmptyDataSetSource
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        return NSAttributedString(string: "No data found", attributes: [NSAttributedString.Key.font: UIFont(name: "Kohinoor Devanagari", size: 19) as Any])
    }

    //MARK: - DZNEmptyDataSetDelegate Methods
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView) -> Bool {
        return true
    }
    
    func emptyDataSetShouldAllowTouch(_ scrollView: UIScrollView) -> Bool {
        return false
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView) -> Bool {
        return true
    }

}
